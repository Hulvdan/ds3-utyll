# Dark Souls 3 Cheat Sheet tool
# Copyright (C) 2018  Aleksey Chistov
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# Source code at https://bitbucket.org/Hulvdan/ds3-utyll

from PyQt5 import QtWidgets, uic, QtGui
from ProfileDS3 import *
from CustomModel import *
import FileData
import json
import os


class DialogExport(QtWidgets.QDialog):
    def __init__(self, parent, flags=QtCore.Qt.WindowCloseButtonHint):
        QtWidgets.QDialog.__init__(self, parent, flags)
        self._gui = parent
        uic.loadUi(FileData.filename_dialog_export, self)

        self._initConnections()
        self._profiles = []
        self._checkboxes_refs_ = {}

        self._initModel()

    def _initModel(self):
        self._model = CustomModel()
        for profile_pair in FileData.getProfiles():
            checkbox = QtGui.QStandardItem()
            self._profiles.append([False, profile_pair[0], checkbox])
            checkbox.setCheckable(True)
            checkbox.setEditable(False)
            checkbox.setText(profile_pair[0])
            self._checkboxes_refs_[id(checkbox)] = self._profiles[-1]
            self._model.appendRow(checkbox)

        self.list.setModel(self._model)
        self._model.stateChanged.connect(self._checkboxFunction)

        double_clicking_lambda = (lambda x: (self._model.itemFromIndex(x).setCheckState(0
                                                    if bool(self._model.itemFromIndex(x).checkState()) else 2)
                                             or self._checkboxFunction(self._model.itemFromIndex(x))))
        self.list.doubleClicked.connect(double_clicking_lambda)
        self._toggleCheckAll()

    def _checkboxFunction(self, item):
        self._checkboxes_refs_[id(item)][0] = True

    def _exportToFile(self):
        export_file_pair = QtWidgets.QFileDialog.getSaveFileName(self._gui, 'Save File', '', 'JSON Data (*.json)')
        if export_file_pair[0] == '':
            return

        checked_profiles = []
        for profile_values in self._profiles:
            if profile_values[0]:
                checked_profiles.append(ProfileDS3(FileData.fileProfile(profile_values[1])))
        if len(checked_profiles) == 0:
            return

        big_data = dict()
        big_data['current'] = self._getCurrentProfileName(FileData.getProfileName(self._gui.getConfig().activeProfile), checked_profiles)

        darksouls3_profiles = dict()
        for profile in checked_profiles:
            profile_name = FileData.getProfileName(profile.filename)
            profile_data = ProfileDS3.dumpProfile(profile)
            darksouls3_profiles[profile_name] = profile_data

        big_data['darksouls3_profiles'] = darksouls3_profiles

        with open(os.path.normpath(export_file_pair[0]), 'w') as export_file:
            json.dump(big_data, export_file)

        self.accept()

    def _exportToClipboard(self):
        checked_profiles = []
        for profile_values in self._profiles:
            if profile_values[0]:
                profile_filename = FileData.fileProfile(profile_values[1])
                profile = ProfileDS3(profile_filename)
                checked_profiles.append(profile)
        if len(checked_profiles) == 0:
            return

        big_data = dict()
        big_data['current'] = self._getCurrentProfileName(FileData.getProfileName(self._gui.getConfig().activeProfile), checked_profiles)

        darksouls3_profiles = dict()
        for profile in checked_profiles:
            profile_name = FileData.getProfileName(profile.filename)
            profile_data = ProfileDS3.dumpProfile(profile)
            darksouls3_profiles[profile_name] = profile_data

        big_data['darksouls3_profiles'] = darksouls3_profiles

        data = json.dumps(big_data)
        QtWidgets.qApp.clipboard().setText(data)
        self.accept()

    @staticmethod
    def _getCurrentProfileName(active_profile_name, selected_profiles) -> str:
        for profile in selected_profiles:
            if active_profile_name == FileData.getProfileName(profile.filename):
                return active_profile_name

        return FileData.getProfileName(selected_profiles[0].filename)


    def _initConnections(self):
        self.pushCheck.pressed.connect(self._toggleCheckAll)
        self.pushExportClip.pressed.connect(self._exportToClipboard)
        self.pushExportFile.pressed.connect(self._exportToFile)

        self.list.verticalScrollBar().valueChanged.connect(self.verticalScrollBarExport.setValue)
        self.verticalScrollBarExport.valueChanged.connect(self.list.verticalScrollBar().setValue)
        self.list.verticalScrollBar().rangeChanged.connect(self.verticalScrollBarExport.setRange)
        self.verticalScrollBarExport.rangeChanged.connect(self.list.verticalScrollBar().setRange)

    def _toggleCheckAll(self):
        all_checked = True
        for pr in self._profiles:
            all_checked *= pr[0]

        for pr in self._profiles:
            pr[0] = not all_checked
            pr[2].setCheckState(0 if all_checked else 2)
