# Dark Souls 3 Cheat Sheet tool
# Copyright (C) 2018  Aleksey Chistov
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# Source code at https://bitbucket.org/Hulvdan/ds3-utyll

from PyQt5 import QtCore, QtGui, QtWidgets


class CustomStyledItemDelegate(QtWidgets.QStyledItemDelegate):
    def __init__(self):
        QtWidgets.QStyledItemDelegate.__init__(self)

    def sizeHint(self,
                 widget: QtWidgets.QStyleOptionViewItem,
                 index: QtCore.QModelIndex):
        """Implementing word wrap"""
        baseSize = super().sizeHint(widget, index)

        tree = widget.styleObject
        item = tree.model().itemFromIndex(index)

        txt = item.text()
        if txt == '':
            return baseSize

        font = widget.font
        metrix = QtGui.QFontMetrics(font)

        k = 1
        height = metrix.height()
        #TODO indentation
        todoINDENTATION = 80
        width_frame = tree.frameRect().width() - todoINDENTATION

        if width_frame < 0:
            return baseSize

        width_text = metrix.width(txt)
        while (width_text/k > width_frame) and (k < 10):
            k += 1

        baseSize.setWidth(width_frame)
        baseSize.setHeight(height*k)
        return baseSize
