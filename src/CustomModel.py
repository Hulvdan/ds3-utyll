# Dark Souls 3 Cheat Sheet tool
# Copyright (C) 2018  Aleksey Chistov
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# Source code at https://bitbucket.org/Hulvdan/ds3-utyll

from PyQt5 import QtGui, QtWidgets, QtCore


class CustomModel(QtGui.QStandardItemModel):
    stateChanged = QtCore.pyqtSignal(QtGui.QStandardItem)
    def __init__(self):
        QtGui.QStandardItemModel.__init__(self)

    def setData(self, QModelIndex, Any, role=None):
        item = self.itemFromIndex(QModelIndex)
        state = bool(item.checkState())
        result = super().setData(QModelIndex, Any, role)
        if state != bool(item.checkState()):
            self.stateChanged.emit(item)
        return result
