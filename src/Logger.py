# Dark Souls 3 Cheat Sheet tool
# Copyright (C) 2018  Aleksey Chistov
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# Source code at https://bitbucket.org/Hulvdan/ds3-utyll

class Logger:
    _level_debug    = 0
    _level_info     = 1
    _level_warning  = 2
    _level_error    = 3
    _level_critical = 4
    _level_release  = 5

    _SEVERITY_LEVEL = _level_debug
    _file = ''

    @staticmethod
    def setLogFile(filename):
        Logger._file = filename
        if Logger._SEVERITY_LEVEL != Logger._level_release:
            open(filename, 'w').close()

    @staticmethod
    def _getSeverityName(level):
        if level == Logger._level_debug:
            return 'debug'
        elif level == Logger._level_info:
            return 'info'
        elif level == Logger._level_warning:
            return 'warning'
        elif level == Logger._level_error:
            return 'Error'
        elif level == Logger._level_critical:
            return 'CRITICAL'
        return ''

    @staticmethod
    def LOGd(tag, string, *args, **kwargs):
        Logger._LOG(Logger._getSeverityName(Logger._level_debug), Logger._level_debug, tag, string, *args, **kwargs)

    @staticmethod
    def LOGi(tag, string, *args, **kwargs):
        Logger._LOG(Logger._getSeverityName(Logger._level_info), Logger._level_info, tag, string, *args, **kwargs)

    @staticmethod
    def LOGw(tag, string, *args, **kwargs):
        Logger._LOG(Logger._getSeverityName(Logger._level_warning), Logger._level_warning, tag, string, *args, **kwargs)

    @staticmethod
    def LOGe(tag, string, *args, **kwargs):
        Logger._LOG(Logger._getSeverityName(Logger._level_error), Logger._level_error, tag, string, *args, **kwargs)

    @staticmethod
    def LOGc(tag, string, *args, **kwargs):
        Logger._LOG(Logger._getSeverityName(Logger._level_critical), Logger._level_critical, tag, string, *args, **kwargs)

    @staticmethod
    def _LOG(severity, level, tag, string, *args, **kwargs):
        if level < Logger._SEVERITY_LEVEL:
            return

        log_string = '{} {}: {}'.format(severity, tag, string.format(*args, **kwargs))
        print(log_string)
        if Logger._file != '':
            with open(Logger._file, 'a') as log_file:
                log_file.write(log_string+'\n')


