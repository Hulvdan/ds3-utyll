# -*- mode: python -*-
import os
import sys

start_path = os.getcwd()
os.chdir('..\\')
sys.path.append(os.path.abspath(r'src'))
import FileData
os.chdir(start_path)
app_ver = FileData.app_version
sys.path = sys.path[:-1]


block_cipher = None
def _paths(*args):
    items = []
    current_dir = os.path.abspath('..\\')
    for file in args:
        filepath = os.path.join(current_dir, os.path.normpath(file))
        if not ('*' in filepath):
            if not os.path.exists(filepath):
                # print('Not Found: ', file)
                continue
            # print('Found: ', file)

        name = filepath.rsplit('.', 1)[0]
        if os.path.sep in filepath:
            name = filepath.rsplit(os.path.sep, 1)[-1]
            _dir = os.path.abspath(filepath.rsplit(os.path.sep, 1)[0])

        extension = name.rsplit('.', 1)[-1]
        if '*' in name:
            directory = os.listdir(os.path.abspath(os.path.join(current_dir, _dir)))
            # print('Checking dir: ' + _dir, *directory, sep='\n\t')
            for filename in directory:
                if len(filename) < len(extension):
                    continue
                file_ext = filename.rsplit('.', 1)[-1]
                if file_ext == extension:
                    items.append((os.path.join(
                        file.rsplit('*', 1)[0] if (file.rsplit('*', 1)[0] != file.rsplit('*', 1)[1]) else '', filename),
                                  os.path.abspath(
                                      os.path.join(current_dir, file.replace('*', filename.rsplit('.')[0]))), 'DATA'))

        else:
            items.append((file, os.path.abspath(os.path.join(current_dir, file)), 'DATA'))
    return items

	
a = Analysis(['..\src\main.py'],
             pathex=[r'.\pyinstaller_build\\'],
             binaries=[],
             datas=[],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=True,
             cipher=block_cipher)
			 
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
				
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          name=FileData.exe_filename,
		  icon=r'..\assets\icon.ico',
          debug=False,
          strip=False,
          upx=True,
          runtime_tmpdir=None,
          console=False , resources=[],
		  version='file_version_info.txt') 
	
datas = _paths(
                r'assets\*.ui', 
				r'assets\*.qss',
				r'assets\*.qfg', 
				r'assets\*.png',
				r'assets\*.ico',
				r'assets\*.json'
				)
		  
coll = COLLECT(exe,
               [],
               [],
               datas,
               [],
               [],
               name='ds3-utyll v'+str(app_ver))
		 