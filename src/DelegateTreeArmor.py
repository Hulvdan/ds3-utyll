# Dark Souls 3 Cheat Sheet tool
# Copyright (C) 2018  Aleksey Chistov
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# Source code at https://bitbucket.org/Hulvdan/ds3-utyll

from PyQt5 import QtCore, QtWidgets, QtGui
from DelegateUniversalTree import *
from ActionCallback import *
import FileData
import json


class DelegateTreeArmor(DelegateUniversalTree):
    _item_dict_string_key = 'string'
    def __init__(self, gui, getItems, tree:QtWidgets.QTreeView, scroll_bar, expanded_items_index_in_profile):
        DelegateUniversalTree.__init__(self, gui, getItems, tree, scroll_bar, expanded_items_index_in_profile)

        with open(FileData.data_armor) as file:
            self._armor_sets = json.load(file)

    def _onUrlItemPress(self, index):
        item = self._action_list.model().itemFromIndex(index)
        self._action_list.clearFocus()
        if not (item is None):
            link = self._action_urls.get(id(item), None)

            if isinstance(link, list):
                self._onArmorCheck(link)
                return

            if not (link is None):
                webbrowser.open(link, new=2, autoraise=False)
                
    def _onArmorCheck(self, sequence:list):
        if not (sequence[0] is None):
            strings = sequence[0].split(' ')
            data = self._gui.getProfile().armor_data['Helms']
            for unit in data:
                if unit.id() in strings:
                    unit.setChecked(sequence[4])

        if not (sequence[1] is None):
            strings = sequence[1].split(' ')
            data = self._gui.getProfile().armor_data['Chests']
            for unit in data:
                if unit.id() in strings:
                    unit.setChecked(sequence[4])

        if not (sequence[2] is None):
            strings = sequence[2].split(' ')
            data = self._gui.getProfile().armor_data['Gauntlets']
            for unit in data:
                if unit.id() in strings:
                    unit.setChecked(sequence[4])

        if not (sequence[3] is None):
            strings = sequence[3].split(' ')
            data = self._gui.getProfile().armor_data['Leggings']
            for unit in data:
                if unit.id() in strings:
                    unit.setChecked(sequence[4])

        self.onItemSelect()
        QtWidgets.qApp.focusWidget().clearFocus()
        self._action_list.clearFocus()
        self._tree.setFocus()
        self.update()

    def update(self):
        super().update()
        self.onItemSelect()

    def _getArmorSet(self, identifier:str):
        armor_type = None
        if 'armors_1_' in identifier:
            armor_type = 'head'
        elif 'armors_2_' in identifier:
            armor_type = 'chest'
        elif 'armors_3_' in identifier:
            armor_type = 'gauntlet'
        elif 'armors_4_' in identifier:
            armor_type = 'leg'

        for armor_set in self._armor_sets:
            if identifier in armor_set[armor_type].split(' '):
                head_id     = armor_set['head']
                chest_id    = armor_set['chest']
                gauntlet_id = armor_set['gauntlet']
                leg_id      = armor_set['leg']

                head_id = (None if (head_id == '') else head_id)
                chest_id = (None if (chest_id == '') else chest_id)
                gauntlet_id = (None if (gauntlet_id == '') else gauntlet_id)
                leg_id = (None if (leg_id == '') else leg_id)

                head_name = None
                chest_name = None
                gauntlet_name = None
                leg_name = None
                if not (head_id is None):
                    data = self._gui.getProfile().armor_data['Helms']
                    name = []
                    for unit in data:
                        for i in head_id.split(' '):
                            if unit.id() == i:
                                name.append(unit.string())
                    head_name = tuple(name)

                if not (chest_id is None):
                    data = self._gui.getProfile().armor_data['Chests']
                    name = []
                    for unit in data:
                        for i in chest_id.split(' '):
                            if unit.id() == i:
                                name.append(unit.string())
                    chest_name = tuple(name)

                if not (gauntlet_id is None):
                    data = self._gui.getProfile().armor_data['Gauntlets']
                    name = []
                    for unit in data:
                        for i in gauntlet_id.split(' '):
                            if unit.id() == i:
                                name.append(unit.string())
                    gauntlet_name = tuple(name)

                if not (leg_id is None):
                    data = self._gui.getProfile().armor_data['Leggings']
                    name = []
                    for unit in data:
                        for i in leg_id.split(' '):
                            if unit.id() == i:
                                name.append(unit.string())
                    leg_name = tuple(name)

                return head_id, chest_id, gauntlet_id, leg_id, head_name, chest_name, gauntlet_name, leg_name

        return None, None, None, None, None, None, None, None

    def onItemSelect(self):
        if not self._gui.actionShowURLslist.isChecked():
            self._action_list.hide()
            return
        index = self._tree.currentIndex()
        if not index.isValid():
            return

        model = self._action_list.model()
        model.clear()
        self._action_urls.clear()

        item = self._model.itemFromIndex(index)
        unit = self._goals_pairs.get(id(item), None)
        if unit is None:
            self._action_urls.clear()
            self._action_list.hide()
            return

        identifier = unit.id()
        head_id, chest_id, gauntlet_id, leg_id, \
        head_name, chest_name, gauntlet_name, leg_name = self._getArmorSet(identifier)

        paired = unit.getHyperlinks()
        added = False
        if self._action_list_min_size_ is None:
            self._action_list_min_size_ = self._action_list.minimumSize()

        font = self._action_list.font()
        metrix = QtGui.QFontMetrics(font)
        w = 0
        h = metrix.height()

        num_none = 0
        for i in (head_id, chest_id, gauntlet_id, leg_id):
            if i is None:
                num_none += 1
        if num_none < 3:
            title_comp = []
            checked = unit.checked()
            title = ('Uncheck Armor Set' if checked else 'Check Armor Set')
            line_prefix = '\n┣ '
            line_prefix_end = '\n┗ '
            if not (head_name is None):
                for name in head_name:
                    title_comp.append(line_prefix + name)

            if not (chest_name is None):
                for i in range(len(chest_name)):
                    condition = (i == len(chest_name) -1 and (gauntlet_name is None or len(gauntlet_name) == 0)
                                and (leg_name is None or len(leg_name) == 0))
                    title_comp.append((line_prefix_end if condition else line_prefix) + chest_name[i])

            if not (gauntlet_name is None):
                for i in range(len(gauntlet_name)):
                    condition = (i == len(gauntlet_name) - 1 and (leg_name is None or len(leg_name) == 0))
                    title_comp.append((line_prefix_end if condition else line_prefix) + gauntlet_name[i])

            if not (leg_name is None):
                for i in range(len(leg_name)):
                    condition = (i == len(leg_name) - 1)
                    title_comp.append((line_prefix_end if condition else line_prefix) + leg_name[i])

            text_width = metrix.width(title)
            for i in title_comp:
                w=max(w, metrix.width(i))
                title += i

            item = QtGui.QStandardItem(title)
            item.setEditable(False)
            model.appendRow(item)
            w = max(w, text_width)
            h += self._action_list.sizeHintForRow(0)


            self._action_urls[id(item)] = [head_id, chest_id, gauntlet_id, leg_id, not checked]

        iterable = zip(paired[0], paired[1], range(len(paired[0])))
        for title, url, row in iterable:
            url_prefix = ''
            string = url_prefix + title
            row += (1 if num_none < 3 else 0)
            added = True
            item = QtGui.QStandardItem(string)
            item.setEditable(False)
            model.appendRow(item)
            self._action_urls[id(item)] = url

            text_width = metrix.width(string)
            w = max(w, text_width)
            h += self._action_list.sizeHintForRow(row)

        widget_width = max(self._action_list_min_size_.width(), w+10)
        widget_height = max(self._action_list_min_size_.height(), h)
        x = self._tree.width()-widget_width
        y = self._tree.height()-widget_height
        self._action_list.setFixedSize(widget_width, widget_height)

        self._action_list.show() if added else (self._action_list.hide() or self._action_urls.clear())
        self._action_list.move(x, y)

    def _openMenu(self, position):
        index = self._tree.currentIndex()

        if not index.isValid():
            return
        item = self._model.itemFromIndex(index)
        unit = self._goals_pairs.get(id(item), None)
        if unit is None:
            return

        identifier = unit.id()
        head_id, chest_id, gauntlet_id, leg_id, \
        head_name, chest_name, gauntlet_name, leg_name = self._getArmorSet(identifier)

        menu = QtWidgets.QMenu()
        num_none = 0
        for i in (head_id, chest_id, gauntlet_id, leg_id):
            if i is None:
                num_none += 1
        if num_none < 3:
            checked = unit.checked()
            title = ('Uncheck Armor Set' if checked else 'Check Armor Set')
            action = ActionCallback(title, self._onArmorCheck, [head_id, chest_id, gauntlet_id, leg_id, not checked], parent=menu)
            menu.addAction(action)

        paired = unit.getHyperlinks()
        for pair in zip(paired[0], paired[1]):
            action = ActionURL(pair[0], pair[1], menu)
            menu.addAction(action)
        menu.exec_(self._tree.viewport().mapToGlobal(position))
        menu.close()
