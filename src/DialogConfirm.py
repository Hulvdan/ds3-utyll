# Dark Souls 3 Cheat Sheet tool
# Copyright (C) 2018  Aleksey Chistov
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# Source code at https://bitbucket.org/Hulvdan/ds3-utyll

from PyQt5 import QtWidgets, uic, QtCore
import FileData


class DialogConfirm(QtWidgets.QDialog):
    @staticmethod
    def execute(text:str, focus_accept:bool=True, parent=None,
                flags=QtCore.Qt.WindowCloseButtonHint | QtCore.Qt.MSWindowsFixedSizeDialogHint) -> bool:
        dialog = DialogConfirm(text, parent, flags, focus_accept)
        result = dialog.exec_()

        return bool(result)
    
    def __init__(self, text, parent, flags, focus_accept):
        QtWidgets.QDialog.__init__(self, parent, flags)
        uic.loadUi(FileData.filename_dialog_confirm, self)
        self.label.setText(text)

        self.pushButtonOK.setFocus() if focus_accept else self.pushButtonCancel.setFocus()
