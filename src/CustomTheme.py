# Dark Souls 3 Cheat Sheet tool
# Copyright (C) 2018  Aleksey Chistov
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# Source code at https://bitbucket.org/Hulvdan/ds3-utyll

from PyQt5 import QtCore, QtWidgets, QtGui
from Logger import *
import FileData
import json
import os


class CustomTheme(QtCore.QObject):
    tag = 'CustomTheme'
    update_stylesheet_everywhere = True

    def __init__(self, gui, update_key=None, remove_key=None):
        QtCore.QObject.__init__(self, gui)
        self.theme_config = dict()

        self._gui = gui
        self._m_update_key = update_key
        self._m_remove_key = remove_key
        if not (update_key is None):
            if CustomTheme.update_stylesheet_everywhere:
                QtWidgets.qApp.installEventFilter(self)
            else:
                self._gui.installEventFilter(self)
        self._updatable_objects = []

    def addUpdatableObject(self, obj):
        self._updatable_objects.append(obj)

    def getFilterListAdditionalSize(self):
        return self.theme_config.get('Filter List Additional Size', 0)

    def eventFilter(self, obj, event:QtCore.QEvent):
        if event.type() == QtCore.QEvent.KeyPress:
            if self._m_update_key == QtGui.QKeySequence(event.key()):
                Logger.LOGi(CustomTheme.tag, "update theme")
                self.updateStyle()
                return True

            elif self._m_remove_key == QtGui.QKeySequence(event.key()):
                self.removeStyle()
                return True
            return False

        return super().eventFilter(obj, event)


    def updateStyle(self):
        path = self._gui.getConfig().activeTheme

        theme_config_path = path.rsplit('.', 1)[0] + '.' + FileData.ext_theme_config
        if os.path.exists(theme_config_path):
            with open(theme_config_path) as file:
                self.theme_config = json.load(file)
        else:
            self.theme_config.clear()

        if not os.path.exists(path):
            Logger.LOGe(__class__.tag, "theme '{}' not found", path)
            return

        with open(path, "r") as theme_file:
            QtWidgets.qApp.setStyleSheet(theme_file.read())

        for obj in self._updatable_objects:
            obj.updateStyle()

    def removeStyle(self):
        QtWidgets.qApp.setStyleSheet("")
        self.theme_config.clear()

        for obj in self._updatable_objects:
            obj.updateStyle()

        Logger.LOGi(__class__.tag, "theme removed")
