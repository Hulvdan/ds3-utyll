# Dark Souls 3 Cheat Sheet tool
# Copyright (C) 2018  Aleksey Chistov
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# Source code at https://bitbucket.org/Hulvdan/ds3-utyll

def formatVersionFile(data,
                      version,
                      exe_name) -> str:

    version_tuple = tuple([int(i) for i in (version.split('.') + ([0] * (4 - len(version.split('.')))))])
    return data.format(version=version,
                       exe_name=exe_name,
                       version_tuple=version_tuple)

def main():
    import os, sys
    start_path = os.getcwd()
    os.chdir('..\\')
    sys.path.append(os.path.abspath('src'))
    import FileData
    os.chdir(start_path)
    sys.path = sys.path[:-1]

    version       = FileData.app_version
    exe_name      = FileData.exe_filename+'.exe'

    input_filename = 'file_version_info_unformatted.txt'
    output_file = 'file_version_info.txt'
    with open(input_filename) as ifile, open(output_file, 'w') as ofile:
        data = ifile.read()
        new_data = formatVersionFile(data, version, exe_name)
        ofile.write(new_data)


if __name__ == '__main__':
    main()
