# Dark Souls 3 Cheat Sheet tool
# Copyright (C) 2018  Aleksey Chistov
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# Source code at https://bitbucket.org/Hulvdan/ds3-utyll

from PyQt5 import QtWidgets, uic, QtGui, QtCore
from Logger import *
import FileData


class DialogMessage(QtWidgets.QDialog):
    tag = 'DialogMessage'

    @staticmethod
    def execute(text, error=False, parent=None, flags=(QtCore.Qt.WindowCloseButtonHint |
                                               QtCore.Qt.MSWindowsFixedSizeDialogHint)):
        dialog = DialogMessage(FileData.filename_dialog_message, text, error, parent, flags)
        dialog.exec_()

    def __init__(self, filename, text, error, parent, flags):
        QtWidgets.QDialog.__init__(self, parent, flags)
        uic.loadUi(filename, self)
        if error:
            self.setWindowTitle(self.windowTitle() + 'Error')
            self.label.setText(text + '\n\nSee log.txt for more information')
            Logger.LOGe(DialogMessage.tag, text.replace('\n', ' '))
        else:
            self.setWindowTitle(self.windowTitle() + 'Message')
            self.label.setText(text)
