# Dark Souls 3 Cheat Sheet tool
# Copyright (C) 2018  Aleksey Chistov
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# Source code at https://bitbucket.org/Hulvdan/ds3-utyll

from PyQt5 import QtCore, QtWidgets, QtGui, Qt
from ModelItemsIter import *
from Logger import *


class FilterItemsListView(QtWidgets.QListView):
    tag = 'FilterItemListView'
    top_offset = 51

    def __init__(self, parent=None):
        QtWidgets.QListView.__init__(self, parent=parent)
        # self.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.setObjectName(__class__.tag)
        self._mouse_inside = False
        self.installEventFilter(self)
        # self.setSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        self.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.setWordWrap(False)
        self._widget = None

    def isMouseInside(self):
        return self._mouse_inside

    def setWidgetForAnchor(self, widget):
        self._widget = widget

    def updatePosition(self):
        if not (self._widget is None):
            pos = self._widget.pos()
            self.move(QtCore.QPoint(pos.x(), pos.y() + self._widget.height() + FilterItemsListView.top_offset))

    def updateSize(self):
        max_width = 0
        font = self.font()
        metrix = QtGui.QFontMetrics(font)
        max_height = 0
        for i in range(self.model().rowCount()):
            size = self.sizeHintForRow(i)
            max_height += size
            max_width = max(max_width, metrix.width(self.model().item(i).text()))

        filter_list_additional_size = self.parent().getCustomTheme().getFilterListAdditionalSize()
        self.setFixedSize(max_width+50, max_height + 4 + filter_list_additional_size)

    def onCheckBoxEnter(self):
        self.setHidden(False)
        self.updatePosition()
        # self._mouse_inside = True
        self.updateSize()

    def onCheckBoxLeave(self):
        if not self._mouse_inside:
            self.setHidden(True)

    def eventFilter(self, obj, event: QtCore.QEvent):
        if (event.type() == QtCore.QEvent.Enter
                or event.type() == QtCore.QEvent.FocusIn):
            # Logger.LOGd(__class__.tag, 'Mouse Enter')
            self._mouse_inside = True
            self.setHidden(False)
            return True

        if (event.type() == QtCore.QEvent.Leave
                or event.type() == QtCore.QEvent.FocusOut):
            # Logger.LOGd(__class__.tag, 'Mouse Leave')
            self._mouse_inside = False
            self.setHidden(True)
            self.clearFocus()
            return True

        return super().eventFilter(obj, event)
