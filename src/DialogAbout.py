# Dark Souls 3 Cheat Sheet tool
# Copyright (C) 2018  Aleksey Chistov
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# Source code at https://bitbucket.org/Hulvdan/ds3-utyll

from PyQt5 import QtWidgets, uic, QtGui, QtCore
from Config import *
import FileData


class DialogAbout(QtWidgets.QDialog):
    def __init__(self, parent, flags=QtCore.Qt.WindowCloseButtonHint |
                             QtCore.Qt.MSWindowsFixedSizeDialogHint):
        QtWidgets.QDialog.__init__(self, parent, flags)
        uic.loadUi(FileData.filename_dialog_about, self)
        self.label_8.setText(str(FileData.app_version))
