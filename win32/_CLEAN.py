
import os
import shutil


def main():
    _deleteDirectory('pyinstaller_build')
    _deleteDirectory('pyinstaller_dist')
    _deleteFile('file_version_info.txt')

def _deleteDirectory(name:str):
    try:
        if os.path.exists(name):
            if os.path.isdir(name):
                shutil.rmtree(name, ignore_errors=True)
                print("Deleted '{}' directory".format(name))
    except PermissionError as error:
        print("Cannot delete '{}' directory. {}".format(name, error))

def _deleteFile(name:str):
    try:
        if os.path.exists(name):
            if os.path.isfile(name):
                os.remove(name)
                print("Deleted '{}' file".format(name))
    except PermissionError as error:
        print("Cannot delete '{}' file. {}".format(name, error))


if __name__ == '__main__':
    main()
    input('\nPress Enter to exit. ')
