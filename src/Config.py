# Dark Souls 3 Cheat Sheet tool
# Copyright (C) 2018  Aleksey Chistov
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# Source code at https://bitbucket.org/Hulvdan/ds3-utyll

from PyQt5 import QtWidgets
import FileData
import os
import json
from Logger import *


class Config:
    tag = 'Config'
    def __init__(self, filename):
        self.filename = filename
        self._currentTab = 0

        resolution = QtWidgets.qApp.desktop().screenGeometry()
        self.rememberWindowSizePos = True
        self.size = (780,580)
        self.position = (resolution.width()/2 - self.size[0]/2, resolution.height()/2 - self.size[1]/2)

        self.activeTheme = FileData.fileThemeDefault()
        self.activeProfile = FileData.fileProfileDefault()
        self._load()

    @staticmethod
    def _isValidFile(filename) -> bool:
        if os.path.isfile(os.path.abspath(filename)):
            with open(filename, 'r') as input_file:
                try:
                    def isPropertyInDictionary(dictionary:dict, property_name:str) -> bool:
                        if dictionary.get(property_name, None) is None:
                            Logger.LOGe(Config.tag, "Not found '{}'", property_name)
                            return False
                        return True

                    data = json.load(input_file)
                    loaded_successfully = (
                        isPropertyInDictionary(data, 'manifest') and
                        isPropertyInDictionary(data, 'remSizePos') and
                        isPropertyInDictionary(data, 'position') and
                        isPropertyInDictionary(data, 'size') and
                        isPropertyInDictionary(data, 'theme') and
                        isPropertyInDictionary(data, 'profile')
                    )

                    return loaded_successfully

                except json.JSONDecodeError as exception:
                    Logger.LOGe(Config.tag, "'{}' JSONDecodeError: {}", filename, exception)
                    return False
        return False


    def _load(self):
        if Config._isValidFile(self.filename):
            with open(self.filename, 'r') as input_file:
                data = json.load(input_file)
                if data['manifest'] == FileData.config_manifest:
                    self.rememberWindowSizePos = data['remSizePos']
                    self.position = data['position']
                    self.size = data['size']
                    self.activeTheme = data['theme']
                    self.activeProfile = data['profile']
                    return

        self.save()

    def save(self):
        with open(self.filename, 'w') as output_file:
            data = {
                'manifest': FileData.config_manifest,
                'size': list(self.size),
                'position': list(self.position),
                'remSizePos': self.rememberWindowSizePos,
                'theme': self.activeTheme,
                'profile': self.activeProfile
            }
            output_file.write(json.dumps(data))
