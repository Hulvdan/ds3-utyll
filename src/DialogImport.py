# Dark Souls 3 Cheat Sheet tool
# Copyright (C) 2018  Aleksey Chistov
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# Source code at https://bitbucket.org/Hulvdan/ds3-utyll

from PyQt5 import QtWidgets, uic, QtGui
from ProfileDS3 import *
from CustomModel import *
from Logger import *
from DialogMessage import *
import FileData
import json
import os


class DialogImport(QtWidgets.QDialog):
    tag = 'DialogImport'
    def __init__(self, parent, flags=QtCore.Qt.WindowCloseButtonHint):
        QtWidgets.QDialog.__init__(self, parent, flags)
        self._gui = parent
        uic.loadUi(FileData.filename_dialog_import, self)

        self._initConnections()
        self._profiles = []
        self._checkboxes_refs_ = {}

        self._import_filename = ''
        self._model = CustomModel()
        self.list.setModel(self._model)
        self._model.stateChanged.connect(self._checkboxFunction)

        double_clicking_lambda = (lambda x: (self._model.itemFromIndex(x).setCheckState(0
                                                    if bool(self._model.itemFromIndex(x).checkState()) else 2)
                                             or self._checkboxFunction(self._model.itemFromIndex(x))))
        self.list.doubleClicked.connect(double_clicking_lambda)


    def _reloadModel(self, big_data):
        self._model.clear()
        self._profiles.clear()
        self._checkboxes_refs_.clear()

        for profile_name, profile_data in big_data['darksouls3_profiles'].items():
            checkbox = QtGui.QStandardItem()
            self._profiles.append([False, profile_name, profile_data, checkbox])
            checkbox.setCheckable(True)
            checkbox.setEditable(False)
            checkbox.setText(profile_name)
            self._checkboxes_refs_[id(checkbox)] = self._profiles[-1]
            self._model.appendRow(checkbox)
        self._toggleCheckAll()

    def _checkboxFunction(self, item):
        self._checkboxes_refs_[id(item)][0] = True

    def _import(self):
        for item in self._profiles:
            if item[0]:
                profile = ProfileDS3.createProfile(item[1], item[2])
                profile.save()

    def accept(self):
        self._import()
        super().accept()

    def _loadFromFile(self):
        importfile_pair = QtWidgets.QFileDialog.getOpenFileName(self._gui, 'Open File',
                                                                '', 'JSON Data (*.json)')
        filename = importfile_pair[0]
        if filename == '':
            self._model.clear()
            return

        self._import_filename = os.path.normpath(filename)
        if ProfileDS3.isValidImportFile(self._import_filename):
            with open(self._import_filename) as file:
                big_data = json.load(file)
                self._reloadModel(big_data)
        else:
            DialogMessage.execute("File '{}'\nisn't valid file for importing profiles".format(self._import_filename), True, self)

    def _loadFromClipboard(self):
        data = QtWidgets.qApp.clipboard().text()
        try:
            big_data = json.loads(data)
            if ProfileDS3.isValidImportData(big_data):
                self._reloadModel(big_data)
                return
            # else:
            #     self._model.clear()
        except json.JSONDecodeError as error:
            Logger.LOGe(DialogImport.tag, '{}', error)
        DialogMessage.execute("Your clipboard data\nisn't valid for importing profiles", True, self)
        self._model.clear()



    def _initConnections(self):
        self.pushLoadFile.pressed.connect(self._loadFromFile)
        self.pushLoadClip.pressed.connect(self._loadFromClipboard)
        self.pushCheck.pressed.connect(self._toggleCheckAll)

        self.list.verticalScrollBar().valueChanged.connect(self.verticalScrollBarImport.setValue)
        self.verticalScrollBarImport.valueChanged.connect(self.list.verticalScrollBar().setValue)
        self.list.verticalScrollBar().rangeChanged.connect(self.verticalScrollBarImport.setRange)
        self.verticalScrollBarImport.rangeChanged.connect(self.list.verticalScrollBar().setRange)


    def _toggleCheckAll(self):
        all_checked = True
        for pr in self._profiles:
            all_checked *= pr[0]

        for pr in self._profiles:
            pr[0] = not all_checked
            pr[3].setCheckState(0 if all_checked else 2)
