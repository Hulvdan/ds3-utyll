# Dark Souls 3 Cheat Sheet tool
# Copyright (C) 2018  Aleksey Chistov
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# Source code at https://bitbucket.org/Hulvdan/ds3-utyll

from PyQt5 import QtWidgets


class ActionCallback(QtWidgets.QAction):
    """Works more stable than just connecting lambdas to Actions"""
    def __init__(self, title, callback, *args, parent=None):
        QtWidgets.QAction.__init__(self, title, parent)
        self.callback_args = args
        self.callback = callback
        self.triggered.connect(lambda: self.callback(*self.callback_args))
