# Dark Souls 3 Cheat Sheet tool
# Copyright (C) 2018  Aleksey Chistov
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# Source code at https://bitbucket.org/Hulvdan/ds3-utyll

from PyQt5 import QtGui, QtCore, QtWidgets


class ModelItemsIter:
    def __init__(self, model:QtGui.QStandardItemModel, item:QtGui.QStandardItem=None):
        self._model = model
        self._last_row = 0
        if item is None:
            self._max_row = model.invisibleRootItem().rowCount()
            self._parent_index = model.index(0, -1)
        else:
            self._max_row = item.rowCount()
            self._parent_index = model.indexFromItem(item)

    def __iter__(self):
        return self

    def __next__(self) -> QtGui.QStandardItem:
        if self._last_row >= self._max_row:
            raise StopIteration

        # column = self._parent_index.column() + 1
        row = self._last_row
        item = self._model.itemFromIndex(self._model.index(row, 0, self._parent_index))
        # print(self._parent_index.row(), self._start_row)

        # if not (item is None):
        #     print("item: '{}', column: {}, row: {}".format(item.text(), column, row))
        # else:
        #     print("item: {}, column: {}, row: {}".format(item, column, row))
        self._last_row += 1
        return item
