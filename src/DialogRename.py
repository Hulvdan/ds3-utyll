# Dark Souls 3 Cheat Sheet tool
# Copyright (C) 2018  Aleksey Chistov
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# Source code at https://bitbucket.org/Hulvdan/ds3-utyll

from PyQt5 import QtWidgets, uic, QtGui, QtCore
from DialogMessage import *
import FileData


class DialogRename(QtWidgets.QDialog):
    def __init__(self, current_name, ui_filename, *args, **kwargs):
        QtWidgets.QDialog.__init__(self, *args, **kwargs)
        uic.loadUi(ui_filename, self)
        self.setWindowTitle(self.windowTitle() + current_name)

    @staticmethod
    def execute(current_name, parent=None) -> str:
        dialog = DialogRename(current_name, FileData.filename_dialog_rename, parent=parent, flags=QtCore.Qt.WindowCloseButtonHint)
        result = dialog.exec_()
        if result == 0:
            return ''
        name = dialog.lineEdit.text()
        dialog.deleteLater()

        if not FileData.isValidFilename(name):
            DialogMessage.execute("'{}' filename is invalid".format(name), True)
            return ''
        return name
