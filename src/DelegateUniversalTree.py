# Dark Souls 3 Cheat Sheet tool
# Copyright (C) 2018  Aleksey Chistov
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# Source code at https://bitbucket.org/Hulvdan/ds3-utyll

from PyQt5 import QtCore, QtWidgets, QtGui, Qt
from CustomModel import *
from CustomStyledItemDelegate import *
from ActionURL import *
from UrlListView import *
from ProfileDS3 import *
from ModelItemsIter import *


class DelegateUniversalTree(QtCore.QObject):
    tag='DelegateUniversalTree'
    _item_dict_string_key = 'name'
    def __init__(self, gui, getItems, tree:QtWidgets.QTreeView, scroll_bar:QtWidgets.QScrollBar, expanded_items_index_in_profile):
        self._getItems = getItems
        QtCore.QObject.__init__(self, gui)

        self._gui = gui
        self._data = self._getItems()
        self._tree = tree
        self._scroll_bar = scroll_bar

        self.expanded_items_index_in_profile = expanded_items_index_in_profile

        self._action_urls = dict()
        self._action_list = UrlListView(tree)
        model = QtGui.QStandardItemModel()
        self._action_list.setModel(model)

        self._goals_pairs = dict()

        self._model = CustomModel()
        self._initTree()
        selection_model = Qt.QItemSelectionModel()
        selection_model.selectionChanged.connect(self.onItemSelect)
        selection_model.setModel(self._model)
        tree.setSelectionModel(selection_model)
        self._action_list_min_size_ = None
        self._initConnections()

    def loadScrollData(self):
        self._tree.repaint()
        k = self._gui.getProfile().trees_scroll[self.expanded_items_index_in_profile]
        max_value = self._tree.verticalScrollBar().maximum()
        scroller_value = k*max_value
        self._scroll_bar.setValue(scroller_value)

    def dumpScrollData(self):
        scroller_value = self._scroll_bar.value()
        scroller_max = self._scroll_bar.maximum()
        k = 0
        if scroller_max != 0:
            k = scroller_value / scroller_max
        self._gui.getProfile().trees_scroll[self.expanded_items_index_in_profile] = k

    def updateStyle(self):
        self.update()
        cfg = self._gui.getCustomTheme().theme_config
        font = self._tree.font()
        size = cfg.get('Tree Items Font Size', 14)
        font.setPointSize(size)
        self._tree.setFont(font)
        self._action_list.setFont(font)
        DelegateUniversalTree._updateCheckboxesStyles(self, cfg)

    def _updateCheckboxesStyles(self, cfg, item:QtGui.QStandardItem=None):
        for child_item in ModelItemsIter(self._model, item):
            if child_item.hasChildren():
                self._updateCheckboxesStyles(cfg, child_item)

        if item is None:
            return

        checked = bool(item.checkState())
        has_children = item.hasChildren()
        DelegateUniversalTree._updateCheckboxStyle(cfg, item, checked, has_children)


    def _onItemActivation(self, index):
        if not self._gui.actionShowURLslist.isChecked():
            return

        item = self._tree.model().itemFromIndex(index)
        if not (item is None):
            unit = self._goals_pairs.get(id(item), None)
            if not (unit is None):
                if len(unit.getHyperlinks()[0]) > 0:
                    self._action_list.setFocus()

    def _getExpandedItems(self):
        return self._gui.getProfile().getExpandedItemsAt(self.expanded_items_index_in_profile)

    def _showCompleted(self):
        return self._gui.actionShowCompleted.isChecked()

    def _isItemNeedHiding(self, item):
        return False

    def _onUrlItemPress(self, index):
        item = self._action_list.model().itemFromIndex(index)
        if not (item is None):
            link = self._action_urls.get(id(item), None)
            if not (link is None):
                self._tree.setFocus()
                self._action_list.clearFocus()
                webbrowser.open(link, new=2, autoraise=False)
            else:
                item_name = item.text()
                Logger.LOGe(DelegateUniversalTree.tag, "Link for '{}' not found", item_name)


    def onItemSelect(self):
        if not self._gui.actionShowURLslist.isChecked():
            self._action_list.hide()
            return

        index = self._tree.currentIndex()
        if not index.isValid():
            return

        model = self._action_list.model()
        model.clear()
        self._action_urls.clear()

        item = self._model.itemFromIndex(index)
        unit = self._goals_pairs.get(id(item), None)
        if unit is None:
            self._action_urls.clear()
            self._action_list.hide()
            return

        paired = unit.getHyperlinks()
        added = False
        if self._action_list_min_size_ is None:
            self._action_list_min_size_ = self._action_list.minimumSize()

        font = self._action_list.font()
        metrix = QtGui.QFontMetrics(font)
        w = 0
        h = 0

        iterable = zip(paired[0], paired[1], range(len(paired[0])))
        for title, url, row in iterable:
            url_prefix = ''
            string = url_prefix + title
            added = True
            item = QtGui.QStandardItem(string)
            item.setEditable(False)
            model.appendRow(item)
            self._action_urls[id(item)] = url

            text_width = metrix.width(string)
            w = max(w, text_width)
            h += self._action_list.sizeHintForRow(row)

        widget_width =  max(self._action_list_min_size_.width(), w+10)
        widget_height = max(self._action_list_min_size_.height(), h+2)
        x = self._tree.width()  - widget_width
        y = self._tree.height() - widget_height
        self._action_list.setFixedSize(widget_width, widget_height)

        self._action_list.show() if added else (self._action_list.hide() or self._action_urls.clear())
        self._action_list.move(x, y)

    def reinit(self):
        self._goals_pairs.clear()
        self._model.clear()
        self._data = self._getItems()
        self._initModel()
        self._action_list.clear()
        self._action_list.hide()
        self.update()

    def _initConnections(self):
        self._tree.collapsed.connect(self._onItemCollapse)
        self._tree.expanded.connect(self._onItemExpand)

        self._tree.verticalScrollBar().valueChanged.connect(self._scroll_bar.setValue)
        self._scroll_bar.valueChanged.connect(self._tree.verticalScrollBar().setValue)
        self._tree.verticalScrollBar().rangeChanged.connect(self._scroll_bar.setRange)
        self._scroll_bar.rangeChanged.connect(self._tree.verticalScrollBar().setRange)

        self._action_list.activated.connect(self._onUrlItemPress)
        self._action_list.clicked.connect(self._onUrlItemPress)

        self._tree.activated.connect(self._onItemActivation)
        self._tree.customContextMenuRequested.connect(self._openMenu)

    @staticmethod
    def _updateCheckboxStyle(cfg:dict, checkbox:QtGui.QStandardItem, completed:bool, isTitle:bool):
        if isTitle:
            _d = cfg.get('TitleCheckbox', None)
        else:
            _d = cfg.get('GoalCheckbox', None)

        if not (_d is None):
            d = _d.get('completed' if completed else 'normal', None)

            if not (d is None):
                style = d['style']
                splitted = style.split(' ')
                s_bold = 'bold' in splitted
                s_italic = 'italic' in splitted
                s_strikeout = 'strikeout' in splitted

                font = checkbox.font()
                font.setBold(s_bold)
                font.setItalic(s_italic)
                font.setStrikeOut(s_strikeout)

                checkbox.setFont(font)


    def _getCheckboxesRecursively(self, value):
        if isinstance(value, dict):
            title_checkboxes = []
            for key, data in value.items():
                title_checkbox = QtGui.QStandardItem()
                title_checkbox.setCheckable(True)
                title_checkbox.takeRow(0)

                massive = self._getCheckboxesRecursively(data)
                for checkbox in massive:
                    if isinstance(checkbox, QtGui.QStandardItem):
                        title_checkbox.appendRow(checkbox)

                title_checkboxes.append(title_checkbox)
                title_checkboxes.append(massive)
            return title_checkboxes

        checkboxes = []
        for unit in value:
            checkbox = QtGui.QStandardItem()

            checkbox.setCheckable(True)
            checkbox.setText(unit.string())

            state = 2 if unit.checked() else 0
            DelegateUniversalTree.safeTitleChecking = True
            checkbox.setCheckState(state)
            DelegateUniversalTree.safeTitleChecking = False

            self._goals_pairs[id(checkbox)] = unit

            checkboxes.append(checkbox)
        return checkboxes


    def _initModel(self):
        # creating all items
        _items = self._getCheckboxesRecursively(self._data)
        for item in _items:
            if isinstance(item, QtGui.QStandardItem):
                self._model.appendRow(item)

    def _initTree(self):
        self._scroll_bar.setRange(self._tree.verticalScrollBar().minimum(),
                                             self._tree.verticalScrollBar().maximum())
        self._scroll_bar.setValue(self._tree.verticalScrollBar().value())

        double_clicking_lambda = (lambda x: (self._model.itemFromIndex(x).setCheckState(0
                                                    if bool(self._model.itemFromIndex(x).checkState()) else 2)
                                             or self._checkboxFunc(self._model.itemFromIndex(x))))
        self._tree.doubleClicked.connect(double_clicking_lambda)
        self._tree.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self._tree.setWordWrap(True)

        delegate = CustomStyledItemDelegate()
        self._tree.setItemDelegate(delegate)

        self._initModel()
        self._tree.setModel(self._model)
        self._model.stateChanged.connect(self._checkboxFunc)

        # self.update()

    def _recursiveUpdate(self, value, item:QtGui.QStandardItem=None):
        _ch_num = 0
        _ch_num_completed = 0
        if isinstance(value, dict):
            for key, data, child_item in zip(value.keys(), value.values(), ModelItemsIter(self._model, item)):
                    child_item.hasChildren()
                    ch_num, ch_num_completed = self._recursiveUpdate(data, child_item)

                    all_checked = True
                    row = 0

                    if child_item.hasChildren():
                        for child_child_item in ModelItemsIter(self._model, child_item):
                            if isinstance(child_child_item, QtGui.QStandardItem):
                                state = child_child_item.checkState()
                                if not self._model.hasChildren(self._model.indexFromItem(child_child_item)):
                                    unit = data[row]
                                    checked = unit.checked()
                                    state = 2 if checked else 0
                                    child_child_item.setCheckState(state)

                                is_item_need_hiding = self._isItemNeedHiding(child_child_item)
                                if not (((not self._showCompleted()) and bool(state)) or is_item_need_hiding):
                                    all_checked *= bool(child_child_item.checkState())
                                    self._tree.setRowHidden(row, self._model.indexFromItem(child_item), False)
                                    if not child_child_item.hasChildren():
                                        ch_num += 1
                                        if bool(child_child_item.checkState()):
                                            ch_num_completed += 1
                                else:
                                    self._tree.setRowHidden(row, self._model.indexFromItem(child_item), True)
                                row += 1

                    child_item.setCheckState(2 if bool(all_checked) else 0)
                    if self._showCompleted():
                        child_item.setText(key+' ('+str(ch_num_completed)+'/'+str(ch_num)+')')
                    elif ch_num > 0:
                        child_item.setText(key+' ('+str(ch_num)+')')
                    else:
                        child_item.setText(key)
                    self._updateCheckboxStyle(self._gui.getCustomTheme().theme_config, child_item, all_checked, True)
                    _ch_num += ch_num
                    _ch_num_completed += ch_num_completed
        else:
            # Goal checkboxes
            for data, item in zip(value, ModelItemsIter(self._model, item)):
                self._updateCheckboxStyle(self._gui.getCustomTheme().theme_config, item, data.checked(), False)

        return _ch_num, _ch_num_completed

    def update(self):
        self._recursiveUpdate(self._data)

        for seq in self._getExpandedItems():
            index = self._model.index(seq[-2], seq[-1])
            for i in range(int(len(seq)/2)-1):
                index = index.child(seq[-i*2], seq[-i*2-1])
            self._tree.expand(index)

        if not self._gui.actionShowURLslist.isChecked():
            self._action_list.hide()

        widget_width = self._action_list.width()
        widget_height = self._action_list.height()
        x = self._tree.width()-widget_width
        y = self._tree.height()-widget_height
        self._action_list.move(x, y)


    _updatePrevent = False
    @QtCore.pyqtSlot(QtGui.QStandardItem)
    def _checkboxFunc(self, item:QtGui.QStandardItem):
        index = self._model.indexFromItem(item)

        if self._model.hasChildren(index):
            self._titleFunction(item)
        else:
            self._goalFunction(item)

    def _titleFunction(self, item:QtGui.QStandardItem):
        update_prevent = DelegateUniversalTree._updatePrevent
        if not DelegateUniversalTree._updatePrevent:
            DelegateUniversalTree._updatePrevent = True

        state = item.checkState()
        index = self._model.indexFromItem(item)
        row = 0
        all_hidden = True
        while True:
            child = item.child(row)

            if child is None:
                break

            if self._tree.isRowHidden(row, index):
                row += 1
                continue
            all_hidden = False
            child.setCheckState(state)
            self._checkboxFunc(child)
            row+=1

        row = 0
        if all_hidden:
            while True:
                child = item.child(row)
                row += 1
                if child is None:
                    break
                if not self._isItemNeedHiding(child):
                    child.setCheckState(state)
                    self._checkboxFunc(child)

        if DelegateUniversalTree._updatePrevent and (not update_prevent):
            self.update()
            DelegateUniversalTree._updatePrevent = False

    def _goalFunction(self, item:QtGui.QStandardItem):
        unit = self._goals_pairs[id(item)]
        state = bool(item.checkState())
        unit.setChecked(state)

        if not DelegateUniversalTree._updatePrevent:
            self.update()


    def expandAll(self):
        # TODO _onItemExpand optimizing. It executes for every item expanded
        self._tree.expandAll()

    def collapseAll(self):
        # TODO _onItemCollapse optimizing. It executes for every item collapsed
        self._tree.collapseAll()

    def _onItemExpand(self, index:QtCore.QModelIndex):
        massive = []
        i = index
        root_index = self._model.indexFromItem(self._model.invisibleRootItem())
        while i != root_index:
            massive.append(i.row())
            massive.append(i.column())
            i = i.parent()

        # Is it already added?
        for point in self._getExpandedItems():
            if len(point) == len(massive):
                every = True
                for a,b in zip(point, massive):
                    if a != b:
                        every = False
                if every:
                    return
        # Not added -> adding
        self._getExpandedItems().append(massive)

    def _onItemCollapse(self, index:QtCore.QModelIndex):
        massive = []
        i = index
        root_index = self._model.indexFromItem(self._model.invisibleRootItem())
        while i != root_index:
            massive.append(i.row())
            massive.append(i.column())
            i = i.parent()

        # Removing if it in massive
        for point in self._getExpandedItems():
            if len(point) == len(massive):
                every = True
                for i in range(len(point)):
                    if point[i] != massive[i]:
                        every = False
                        continue
                if every:
                    self._getExpandedItems().remove(massive)
            continue

    def _openMenu(self, position):
        index = self._tree.currentIndex()

        if not index.isValid():
            return
        item = self._model.itemFromIndex(index)
        unit = self._goals_pairs.get(id(item), None)
        if unit is None:
            return

        paired = unit.getHyperlinks()
        menu = QtWidgets.QMenu()
        for pair in zip(paired[0], paired[1]):
            action = ActionURL(pair[0], pair[1], menu)
            menu.addAction(action)
        menu.exec_(self._tree.viewport().mapToGlobal(position))
        menu.close()
