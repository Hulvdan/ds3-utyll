# Dark Souls 3 Cheat Sheet tool
# Copyright (C) 2018  Aleksey Chistov
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# Source code at https://bitbucket.org/Hulvdan/ds3-utyll

from Logger import *
from DialogConfirm import *
import FileData
import hlp
import urllib.error
import urllib.request
import json
import os


class ProfileDS3:
    tag = 'Profile'
    def __init__(self, filename, isNew=False):
        self.filename = filename

        self.playthrough_data = {}
        self.achievements_data = {}
        self.weaponsandshields_data = {}
        self.armor_data = {}
        self.misc_data = []

        self.journey = 1
        self.showCompleted   = True
        self.showBosses      = True
        self.showNPCs        = True
        self.showEstusShards = True
        self.showBones       = True
        self.showTomes       = True
        self.showCoals       = True
        self.showAshes       = True
        self.showGestures    = True
        self.showSorceries   = True
        self.showPyromancies = True
        self.showMiracles    = True
        self.showRings       = True
        self.showWeapons     = True
        self.showArmor       = True
        self.showTitanite    = True
        self.showGems        = True
        self.showCovenants   = True
        self.showMiscItems   = True

        self._current_tab = 0
        self.expanded_tree_items = [[], [], [], [], []]
        self.trees_scroll = [0] * 5
        self.show_urls_list = True

        self._load(isNew)

    def _createFilteringDict(self) -> dict:
        filtering_dict = dict()

        filtering_dict['completed'] = self.showCompleted
        filtering_dict["f_boss"]  = self.showBosses
        filtering_dict["f_npc"]   = self.showNPCs
        filtering_dict["f_estus"] = self.showEstusShards
        filtering_dict["f_bone"]  = self.showBones
        filtering_dict["f_tome"]  = self.showTomes
        filtering_dict["f_coal"]  = self.showCoals
        filtering_dict["f_ash"]   = self.showAshes
        filtering_dict["f_gest"]  = self.showGestures
        filtering_dict["f_sorc"]  = self.showSorceries
        filtering_dict["f_pyro"]  = self.showPyromancies
        filtering_dict["f_mirac"] = self.showMiracles
        filtering_dict["f_ring"]  = self.showRings
        filtering_dict["f_weap"]  = self.showWeapons
        filtering_dict["f_arm"]   = self.showArmor
        filtering_dict["f_tit"]   = self.showTitanite
        filtering_dict["f_gem"]   = self.showGems
        filtering_dict["f_cov"]   = self.showCovenants
        filtering_dict["f_misc"]  = self.showMiscItems

        return filtering_dict

    def _load(self, isNew):
        if isNew or (not ProfileDS3._isValidFile(self.filename)):
            self._create()
            return

        with open(self.filename, 'r') as input_file:
            data = json.load(input_file)
            self.playthrough_data = self._loadPlaythroughData(data['Playthrough Checklists'])
            self.achievements_data = self._loadAchievementData(data['Achievement Checklists'])
            self.weaponsandshields_data = self._loadWeaponsData(data['Weapons/Shields Checklists'])
            self.armor_data = self._loadArmorData(data['Armor Checklists'])
            self.misc_data = self._loadMiscData(data['Misc Checklists'])
            self.expanded_tree_items = data['expanded_tree_items']
            self._current_tab = data['current_tab']
            self.journey = data['journey']

            filtering_dict = data.get('filtering_dict', dict())
            self.showCompleted   = filtering_dict.get('completed', True)
            self.showBosses      = filtering_dict.get("f_boss", True)
            self.showNPCs        = filtering_dict.get("f_npc", True)
            self.showEstusShards = filtering_dict.get("f_estus", True)
            self.showBones       = filtering_dict.get("f_bone", True)
            self.showTomes       = filtering_dict.get("f_tome", True)
            self.showCoals       = filtering_dict.get("f_coal", True)
            self.showAshes       = filtering_dict.get("f_ash", True)
            self.showGestures    = filtering_dict.get("f_gest", True)
            self.showSorceries   = filtering_dict.get("f_sorc", True)
            self.showPyromancies = filtering_dict.get("f_pyro", True)
            self.showMiracles    = filtering_dict.get("f_mirac", True)
            self.showRings       = filtering_dict.get("f_ring", True)
            self.showWeapons     = filtering_dict.get("f_weap", True)
            self.showArmor       = filtering_dict.get("f_arm", True)
            self.showTitanite    = filtering_dict.get("f_tit", True)
            self.showGems        = filtering_dict.get("f_gem", True)
            self.showCovenants   = filtering_dict.get("f_cov", True)
            self.showMiscItems   = filtering_dict.get("f_misc", True)

            self.show_urls_list = data['urlslist']

            manifest = data['manifest']
            if manifest != FileData.profile_manifest:
                data['manifest'] = FileData.profile_manifest
                Logger.LOGi(ProfileDS3.tag, "updating '{}': previous manifest - '{}', new - '{}'",
                            FileData.getProfileName(self.filename), manifest, FileData.profile_manifest)
                self.updateProfileContent()

    def save(self):
        folder = os.path.abspath(FileData.profile_folder)
        if not os.path.exists(folder):
            os.mkdir(folder)

        with open(self.filename, 'w') as output_file:
            data = {
                'manifest': FileData.profile_manifest,
                'urlslist': self.show_urls_list,
                'Playthrough Checklists': self._dumpPlaythroughData(self.playthrough_data),
                'Achievement Checklists': self._dumpAchievementData(self.achievements_data),
                'Weapons/Shields Checklists': self._dumpWeaponsData(self.weaponsandshields_data),
                'Armor Checklists': self._dumpArmorData(self.armor_data),
                'Misc Checklists': self._dumpMiscData(self.misc_data),
                'expanded_tree_items': self.expanded_tree_items,
                'current_tab': self._current_tab,
                'filtering_dict': self._createFilteringDict(),
                'journey': self.journey
            }
            output_file.write(json.dumps(data, indent='\t'))
            Logger.LOGi(ProfileDS3.tag, "saved '{}'", FileData.getProfileName(self.filename))

    def _create(self):
        with open(FileData.ds3_data_profile + '.' + FileData.ext_profile, 'r') as data_file:
            data = json.load(data_file)
            self.playthrough_data = self._createPlaythroughData(data['Playthrough Checklists'])
            self.achievements_data = self._createAchievementData(data['Achievement Checklists'])
            self.weaponsandshields_data = self._createWeaponsData(data['Weapons/Shields Checklists'])
            self.armor_data = self._createArmorData(data['Armor Checklists'])
            self.misc_data = self._createMiscData(data['Misc Checklists'])


    @staticmethod
    def isValidImportFile(filename:str) -> bool:
        Logger.LOGi(ProfileDS3.tag, "validating '{}'", filename)
        if os.path.exists(filename):
            with open(filename, 'r') as import_file:
                try:
                    big_data = json.loads(import_file.read())
                    return ProfileDS3.isValidImportData(big_data)

                except json.JSONDecodeError as exception:
                    Logger.LOGe(ProfileDS3.tag, "File '{}' JSONDecodeError: {}", import_file, exception)

        return False

    @staticmethod
    def isValidImportData(big_data:dict) -> bool:
        profiles_dict = big_data.get('darksouls3_profiles', None)

        if profiles_dict is None:
            Logger.LOGe(ProfileDS3.tag, "no 'darksouls3_profiles' in clipboard data")
            return False

        for profile_name, profile_data in profiles_dict.items():
            if not isinstance(profile_data, dict):
                Logger.LOGe(ProfileDS3.tag, "profile_data in '{}' profile isn't a dictionary", profile_name)
                return False

            def isPropertyInDictionary(dictionary:dict, property_name:str) -> bool:
                if dictionary.get(property_name, None) is None:
                    Logger.LOGe(ProfileDS3.tag, "not found '{}' in '{}' profile", property_name, profile_name)
                    return False
                return True

            success = (
                    isPropertyInDictionary(profile_data, 'checklistData') and
                    isPropertyInDictionary(profile_data, 'state') and
                    isPropertyInDictionary(profile_data, 'collapsed') and
                    isPropertyInDictionary(profile_data, 'current_tab') and
                    isPropertyInDictionary(profile_data, 'hide_completed') and
                    isPropertyInDictionary(profile_data, 'hidden_categories') and
                    isPropertyInDictionary(profile_data, 'journey')
            )

            if not success:
                return False

            if not isinstance(profile_data['checklistData'], dict):
                Logger.LOGe(ProfileDS3.tag, "'checklistData' key of '{}' profile isn't a dictionary", profile_name)
                success = False
            if not isinstance(profile_data['state'], dict):
                Logger.LOGe(ProfileDS3.tag, "'state' key of '{}' profile isn't a dictionary", profile_name)
                success = False
            if not isinstance(profile_data['collapsed'], dict):
                Logger.LOGe(ProfileDS3.tag, "'collapsed' key of '{}' profile isn't a dictionary", profile_name)
                success = False
            if not isinstance(profile_data['hidden_categories'], dict):
                Logger.LOGe(ProfileDS3.tag, "'hidden_categories' key of '{}' profile isn't a dictionary", profile_name)
                success = False
            if not isinstance(profile_data['current_tab'], str):
                Logger.LOGe(ProfileDS3.tag, "'current_tab' key of '{}' profile isn't a string", profile_name)
                success = False
            if not isinstance(profile_data['hide_completed'], bool):
                Logger.LOGe(ProfileDS3.tag, "'hide_completed' key of '{}' profile isn't a boolean", profile_name)
                success = False
            if not isinstance(profile_data['journey'], int):
                Logger.LOGe(ProfileDS3.tag, "'journey' key of '{}' profile isn't an integer", profile_name)
                success = False


            if not success:
                return False

            for data_key, data_value in profile_data['checklistData'].items():
                if not isinstance(data_key, str):
                    Logger.LOGe(ProfileDS3.tag, "'{}' key in 'checklistData' of '{}' profile isn't a string",
                                data_key, profile_name)
                    success = False
                if not isinstance(data_value, bool):
                    big_string = "value('{}') of '{}' key in 'checklistData' of '{}' profile isn't a boolean"
                    Logger.LOGe(ProfileDS3.tag, big_string, data_value, data_key, profile_name)
                    success = False

            for data_key, data_value in profile_data['collapsed'].items():
                if not isinstance(data_key, str):
                    Logger.LOGe(ProfileDS3.tag, "'{}' key in 'collapsed' of '{}' profile isn't a string",
                                data_key, profile_name)
                    success = False
                if not isinstance(data_value, bool):
                    Logger.LOGe(ProfileDS3.tag, "value('{}') of '{}' key in 'collapsed' of '{}' profile isn't a boolean",
                                data_value, data_key, profile_name)
                    success = False

            for data_key, data_value in profile_data['hidden_categories'].items():
                if not isinstance(data_key, str):
                    Logger.LOGe(ProfileDS3.tag, "'{}' key in 'hidden_categories' of '{}' profile isn't a string",
                                data_key, profile_name)
                    success = False
                if not isinstance(data_value, bool):
                    big_string = "value('{}') of '{}' key in 'hidden_categories' of '{}' profile isn't a boolean"
                    Logger.LOGe(ProfileDS3.tag, big_string, data_value, data_key, profile_name)
                    success = False

            if not isinstance(profile_data['current_tab'], str):
                Logger.LOGe(ProfileDS3.tag, "value('{}') of 'current_tab' of '{}' profile isn't a string",
                            profile_data['current_tab'], profile_name)
                success = False

            if not isinstance(profile_data['hide_completed'], bool):
                Logger.LOGe(ProfileDS3.tag, "value('{}') of 'hide_completed' of '{}' profile isn't a boolean",
                            profile_data['hide_completed'], profile_name)
                success = False

            if not success:
                return False

        return True

    @staticmethod
    def dumpProfile(profile) -> dict:
        profile_data = dict()
        profile_data['checklistData'] = ProfileDS3._dumpChecklistData(profile)
        profile_data['state'] = dict()
        profile_data['collapsed'] = ProfileDS3._dumpCollapsed(profile)
        profile_data["current_tab"] = ProfileDS3._getCurrentTabToExport(profile)
        profile_data['hide_completed'] = not bool(profile.showCompleted)
        profile_data['hidden_categories'] = ProfileDS3._dumpHiddenCategories(profile)
        profile_data['journey'] = profile.journey
        return profile_data

    @staticmethod
    def _dumpChecklistData(profile) -> dict:
        checklistData = dict()

        for massive in profile.playthrough_data.values():
            for unit in massive:
                state = unit.checked()
                if state:
                    checklistData[unit.id()] = True
        for massive in profile.achievements_data.values():
            if isinstance(massive, dict):
                for l in massive.values():
                    for unit in l:
                        state = unit.checked()
                        if state:
                            checklistData[unit.id()] = True
                continue
            for unit in massive:
                state = unit.checked()
                if state:
                    checklistData[unit.id()] = True
        for dictionary in profile.weaponsandshields_data.values():
            for massive in dictionary.values():
                for unit in massive:
                    state = unit.checked()
                    if state:
                        checklistData[unit.id()] = True
        for massive in profile.armor_data.values():
            for unit in massive:
                state = unit.checked()
                if state:
                    checklistData[unit.id()] = True
        for unit in profile.misc_data:
            state = unit.checked()
            if state:
                checklistData[unit.id()] = True

        return checklistData

    @staticmethod
    def _getCurrentTabToExport(profile) -> int:
        tabs = ('#tabPlaythrough',
                '#tabChecklists',
                '#tabWeaponsShields',
                '#tabArmors',
                '#tabMisc')
        return tabs[profile.getCurrentTab()]

    @staticmethod
    def _dumpHiddenCategories(profile) -> dict:
        hidden_categories = dict()
        # hidden_categories[]

        # filtering_dict['completed'] = self.showCompleted
        hidden_categories["f_boss"]  = not profile.showBosses
        hidden_categories["f_npc"]   = not profile.showNPCs
        hidden_categories["f_estus"] = not profile.showEstusShards
        hidden_categories["f_bone"]  = not profile.showBones
        hidden_categories["f_tome"]  = not profile.showTomes
        hidden_categories["f_coal"]  = not profile.showCoals
        hidden_categories["f_ash"]   = not profile.showAshes
        hidden_categories["f_gest"]  = not profile.showGestures
        hidden_categories["f_sorc"]  = not profile.showSorceries
        hidden_categories["f_pyro"]  = not profile.showPyromancies
        hidden_categories["f_mirac"] = not profile.showMiracles
        hidden_categories["f_ring"]  = not profile.showRings
        hidden_categories["f_weap"]  = not profile.showWeapons
        hidden_categories["f_arm"]   = not profile.showArmor
        hidden_categories["f_tit"]   = not profile.showTitanite
        hidden_categories["f_gem"]   = not profile.showGems
        hidden_categories["f_cov"]   = not profile.showCovenants
        hidden_categories["f_misc"]  = not profile.showMiscItems

        # hidden_categories["f_quest"] = not bool(profile.pl_checking[1])
        # hidden_categories["f_npc"]   = not bool(profile.pl_checking[2])
        # hidden_categories["f_estus"] = not bool(profile.pl_checking[3])
        # hidden_categories["f_gear"]  = not bool(profile.pl_checking[4])
        # hidden_categories["f_ring"]  = not bool(profile.pl_checking[5])
        # hidden_categories["f_spell"] = not bool(profile.pl_checking[6])
        # hidden_categories["f_mat"]   = not bool(profile.pl_checking[7])
        # hidden_categories["f_misc"]  = not bool(profile.pl_checking[8])
        return hidden_categories

    @staticmethod
    def _dumpCollapsed(profile) -> dict:
        collapsed = dict()

        # Locations
        collapsed['#Cemetery_of_Ash_col']               = not ([0,  0] in profile.expanded_tree_items[0])
        collapsed['#Firelink_Shrine_col']               = not ([1,  0] in profile.expanded_tree_items[0])
        collapsed['#High_Wall_of_Lothric_col']          = not ([2,  0] in profile.expanded_tree_items[0])
        collapsed['#Undead_Settlement_col']             = not ([3,  0] in profile.expanded_tree_items[0])
        collapsed['#Road_of_Sacrifices_col']            = not ([4,  0] in profile.expanded_tree_items[0])
        collapsed['#Cathedral_of_the_Deep_col']         = not ([5,  0] in profile.expanded_tree_items[0])
        collapsed['#Farron_Keep_col']                   = not ([6,  0] in profile.expanded_tree_items[0])
        collapsed['#Catacombs_of_Carthus_col']          = not ([7,  0] in profile.expanded_tree_items[0])
        collapsed['#Smouldering_Lake_col']              = not ([8,  0] in profile.expanded_tree_items[0])
        collapsed['#Irithyll_of_the_Boreal_Valley_col'] = not ([9,  0] in profile.expanded_tree_items[0])
        collapsed['#Anor_Londo_col']                    = not ([10, 0] in profile.expanded_tree_items[0])
        collapsed['#Irithyll_Dungeon_col']              = not ([11, 0] in profile.expanded_tree_items[0])
        collapsed['#Profaned_Capital_col']              = not ([12, 0] in profile.expanded_tree_items[0])
        collapsed['#Consumed_Kings_Garden_col']         = not ([13, 0] in profile.expanded_tree_items[0])
        collapsed['#Untended_Graves_col']               = not ([14, 0] in profile.expanded_tree_items[0])
        collapsed['#Archdragon_Peak_col']               = not ([15, 0] in profile.expanded_tree_items[0])
        collapsed['#Lothric_Castle_col']                = not ([16, 0] in profile.expanded_tree_items[0])
        collapsed['#Grand_Archives_col']                = not ([17, 0] in profile.expanded_tree_items[0])
        collapsed['#Kiln_of_the_First_Flame_col']       = not ([18, 0] in profile.expanded_tree_items[0])
        collapsed['#Painted_World_of_Ariandel_col']     = not ([19, 0] in profile.expanded_tree_items[0])
        collapsed['#The_Dreg_Heap_col']                 = not ([20, 0] in profile.expanded_tree_items[0])
        collapsed['#The_Ringed_City_col']               = not ([21, 0] in profile.expanded_tree_items[0])

        # achievements
        collapsed["#Master_of_Expression_col"]   = not ([0,  0] in profile.expanded_tree_items[1])
        collapsed["#Master_of_Sorceries_col"]    = not ([1,  0] in profile.expanded_tree_items[1])
        collapsed["#Master_of_Pyromancies_col"]  = not ([2,  0] in profile.expanded_tree_items[1])
        collapsed["#Master_of_Miracles_col"]     = not ([3,  0] in profile.expanded_tree_items[1])
        collapsed["#DLC_Spells_col"]             = not ([4,  0] in profile.expanded_tree_items[1])
        collapsed["#Master_of_Rings_col"]        = not ([5,  0] in profile.expanded_tree_items[1])
        collapsed["#DLC_Rings_col"]              = not ([6,  0] in profile.expanded_tree_items[1])
        collapsed["#Master_of_Infusion_col"]     = not ([7,  0] in profile.expanded_tree_items[1])
        collapsed["#Ending_Achievements_col"]    = not ([8,  0] in profile.expanded_tree_items[1])
        collapsed["#Boss_Achievements_col"]      = not ([9,  0] in profile.expanded_tree_items[1])
        collapsed["#Misc_Achievements_col"]      = not ([10, 0] in profile.expanded_tree_items[1])
        collapsed["#Covenants_Achievements_col"] = not ([11, 0] in profile.expanded_tree_items[1])

        #weapons
        collapsed["#Weapons_col"] = not ([0, 0] in profile.expanded_tree_items[2])
        collapsed['#Shields_col'] = not ([1, 0] in profile.expanded_tree_items[2])

        #armor
        collapsed['#Helms_col']     = not ([0, 0] in profile.expanded_tree_items[3])
        collapsed['#Chests_col']    = not ([1, 0] in profile.expanded_tree_items[3])
        collapsed['#Gauntlets_col'] = not ([2, 0] in profile.expanded_tree_items[3])
        collapsed['#Leggings_col']  = not ([3, 0] in profile.expanded_tree_items[3])

        #misc
        collapsed['#Pickle_Pee_Pump-a-Rum_Crow_col'] = False
        collapsed['#Soul_Types_col'] = False
        return collapsed

    @staticmethod
    def downloadFile(filename) -> bool:
        """Returns True if the file was downloaded"""
        Logger.LOGi(ProfileDS3.tag, "Downloading '{}' to '{}'", FileData.remote_profile_link, filename)
        try:
            urllib.request.urlretrieve(FileData.remote_profile_link, filename)
            return True

        except urllib.error.URLError as error:
            Logger.LOGe(ProfileDS3.tag, "Error opening URL '{}'. Exception: '{}'", FileData.remote_profile_link, error)

        return False

    @staticmethod
    def updateProfileCreatingFile() -> int:
        # 0 - json couldn't be decoded or couldn't download
        # 1 - higher version of program is required
        # 2 - no updates
        # 3 - file was successfully updaated
        new_datafile = FileData.getNewProfileDataFilename()
        current_datafile = FileData.getProfileDataFilename()

        Logger.LOGi(ProfileDS3.tag, "Downloading '{}' to '{}'", FileData.remote_profile_link, new_datafile)
        try:
            urllib.request.urlretrieve(FileData.remote_profile_link, new_datafile)
            # new_datafile = "assets\\ds3_profile_1.json" # for testing

            with open(new_datafile) as file:
                json_data_serialised = json.load(file)
                new_manifest = json_data_serialised['manifest']

            if new_manifest != FileData.profile_manifest:
                FileData.profile_manifest = new_manifest
                min_app_version = json_data_serialised.get('min_app_version', '1.2.2')

                if not hlp.isVersionBiggerOrEqualThan(FileData.app_version, min_app_version):
                    Logger.LOGe(ProfileDS3.tag, "Latest version of profile data is not supported by this ds3-utyll version. It requires '{}' version of ds3-utyll. Check source for releases (URL is in about window)".format(min_app_version))
                    os.remove(new_datafile)
                    return 1

                result = DialogConfirm.execute("Keep old version of profile data file\nas '{}'?".format(new_datafile), False)
                if result: # Keep previous datafile. Just swap them
                    with open(new_datafile) as nfile, open(current_datafile) as cfile:
                        new_datafile_data = nfile.read()
                        current_datafile_data = cfile.read()
                        Logger.LOGi(ProfileDS3.tag, "'{}' file updated. Keeping old one as '{}'.", current_datafile, new_datafile)

                    with open(new_datafile, 'w') as nfile, open(current_datafile, 'w') as cfile:
                        nfile.write(current_datafile_data)
                        cfile.write(new_datafile_data)
                else: # Don't keep previous datafile
                    Logger.LOGi(ProfileDS3.tag, "'{}' file updated. Deleting old one.", current_datafile)
                    os.remove(current_datafile)
                    os.rename(new_datafile, current_datafile)

                return 3
            else:
                Logger.LOGi(ProfileDS3.tag, "No updates to profile data file.", current_datafile)
                os.remove(new_datafile)
                return 2

        except urllib.error.URLError as error:
            Logger.LOGe(ProfileDS3.tag, "Error opening URL '{}'. Exception: '{}'", FileData.remote_profile_link, error)
            os.remove(new_datafile)
        return 0

    @staticmethod
    def createProfile(profile_name:str, profile_data:dict):
        new_profile = ProfileDS3(FileData.fileProfile(profile_name), True)

        ProfileDS3._processData(new_profile, profile_data['checklistData'])
        new_profile.expanded_tree_items = ProfileDS3._getExpanded(profile_data['collapsed'])
        new_profile.setCurrentTab(ProfileDS3._getCurrentTab(profile_data['current_tab']))
        new_profile._initHiddenCategories(profile_data['hidden_categories'])
        # new_profile.pl_checking = Profile._getPlChecking()

        return new_profile

    @staticmethod
    def _getCurrentTab(current_tab):
        if current_tab == '#tabPlaythrough':
            return 0
        if current_tab == '#tabChecklists':
            return 1
        if current_tab == '#tabWeaponsShields':
            return 2
        if current_tab == '#tabArmors':
            return 3
        if current_tab == '#tabMisc':
            return 4
        return 0

    @staticmethod
    def _processData(profile, checklist_data:dict):
        ProfileDS3._processPlaythroughData(profile, checklist_data)
        ProfileDS3._processAchievementData(profile, checklist_data)
        ProfileDS3._processWeaponsData(profile, checklist_data)
        ProfileDS3._processArmorData(profile, checklist_data)
        ProfileDS3._processMiscData(profile, checklist_data)

    @staticmethod
    def _processPlaythroughData(profile, checklist_data:dict):
        for check_id, boolean in checklist_data.items():
            if boolean:
                processed = False
                for massive in profile.playthrough_data.values():
                    for unit in massive:
                        if unit.id() == check_id:
                            unit.setChecked(boolean)
                            processed = True
                            break
                    if processed:
                        break

    @staticmethod
    def _processAchievementData(profile, checklist_data:dict):
        for check_id, boolean in checklist_data.items():
            if boolean:
                processed = False
                for massive in profile.achievements_data.values():
                    if isinstance(massive, dict):
                        for l in massive.values():
                            for unit in l:
                                if unit.id() == check_id:
                                    unit.setChecked(boolean)
                                    processed = True
                                    break
                            if processed:
                                break
                    else:
                        for unit in massive:
                            if unit.id() == check_id:
                                unit.setChecked(boolean)
                                processed = True
                                break
                    if processed:
                        break

    @staticmethod
    def _processWeaponsData(profile, checklist_data:dict):
        for check_id, boolean in checklist_data.items():
            if boolean:
                processed = False
                for massive in profile.weaponsandshields_data.values():
                    for l in massive.values():
                        for unit in l:
                            if unit.id() == check_id:
                                unit.setChecked(boolean)
                                processed = True
                                break
                        if processed:
                            break
                    if processed:
                        break

    @staticmethod
    def _processArmorData(profile, checklist_data:dict):
        for check_id, boolean in checklist_data.items():
            if boolean:
                processed = False
                for massive in profile.armor_data.values():
                    for unit in massive:
                        if unit.id() == check_id:
                            unit.setChecked(boolean)
                            processed = True
                            break
                    if processed:
                        break

    @staticmethod
    def _processMiscData(profile, checklist_data:dict):
        for check_id, boolean in checklist_data.items():
            if boolean:
                for unit in profile.misc_data:
                    if unit.id() == check_id:
                        unit.setChecked(boolean)
                        break

    def _initHiddenCategories(self, hidden_categories:dict):
        self.showBosses      = not hidden_categories["f_boss"]
        self.showNPCs        = not hidden_categories["f_npc"]
        self.showEstusShards = not hidden_categories["f_estus"]
        self.showBones       = not hidden_categories["f_bone"]
        self.showTomes       = not hidden_categories["f_tome"]
        self.showCoals       = not hidden_categories["f_coal"]
        self.showAshes       = not hidden_categories["f_ash"]
        self.showGestures    = not hidden_categories["f_gest"]
        self.showSorceries   = not hidden_categories["f_sorc"]
        self.showPyromancies = not hidden_categories["f_pyro"]
        self.showMiracles    = not hidden_categories["f_mirac"]
        self.showRings       = not hidden_categories["f_ring"]
        self.showWeapons     = not hidden_categories["f_weap"]
        self.showArmor       = not hidden_categories["f_arm"]
        self.showTitanite    = not hidden_categories["f_tit"]
        self.showGems        = not hidden_categories["f_gem"]
        self.showCovenants   = not hidden_categories["f_cov"]
        self.showMiscItems   = not hidden_categories["f_misc"]

    @staticmethod
    def _getExpanded(collapsed:dict):
        expanded_tree_items = [[],[],[],[],[]]

        # Locations
        expanded_tree_items[0].append([0,  0]) if (not collapsed.get('#Cemetery_of_Ash_col', True))               else None
        expanded_tree_items[0].append([1,  0]) if (not collapsed.get('#Firelink_Shrine_col', True))               else None
        expanded_tree_items[0].append([2,  0]) if (not collapsed.get('#High_Wall_of_Lothric_col', True))          else None
        expanded_tree_items[0].append([3,  0]) if (not collapsed.get('#Undead_Settlement_col', True))             else None
        expanded_tree_items[0].append([4,  0]) if (not collapsed.get('#Road_of_Sacrifices_col', True))            else None
        expanded_tree_items[0].append([5,  0]) if (not collapsed.get('#Cathedral_of_the_Deep_col', True))         else None
        expanded_tree_items[0].append([6,  0]) if (not collapsed.get('#Farron_Keep_col', True))                   else None
        expanded_tree_items[0].append([7,  0]) if (not collapsed.get('#Catacombs_of_Carthus_col', True))          else None
        expanded_tree_items[0].append([8,  0]) if (not collapsed.get('#Smouldering_Lake_col', True))              else None
        expanded_tree_items[0].append([9,  0]) if (not collapsed.get('#Irithyll_of_the_Boreal_Valley_col', True)) else None
        expanded_tree_items[0].append([10, 0]) if (not collapsed.get('#Anor_Londo_col', True))                    else None
        expanded_tree_items[0].append([11, 0]) if (not collapsed.get('#Irithyll_Dungeon_col', True))              else None
        expanded_tree_items[0].append([12, 0]) if (not collapsed.get('#Profaned_Capital_col', True))              else None
        expanded_tree_items[0].append([13, 0]) if (not collapsed.get('#Consumed_Kings_Garden_col', True))         else None
        expanded_tree_items[0].append([14, 0]) if (not collapsed.get('#Untended_Graves_col', True))               else None
        expanded_tree_items[0].append([15, 0]) if (not collapsed.get('#Archdragon_Peak_col', True))               else None
        expanded_tree_items[0].append([16, 0]) if (not collapsed.get('#Lothric_Castle_col', True))                else None
        expanded_tree_items[0].append([17, 0]) if (not collapsed.get('#Grand_Archives_col', True))                else None
        expanded_tree_items[0].append([18, 0]) if (not collapsed.get('#Kiln_of_the_First_Flame_col', True))       else None
        expanded_tree_items[0].append([19, 0]) if (not collapsed.get('#Painted_World_of_Ariandel_col', True))     else None
        expanded_tree_items[0].append([20, 0]) if (not collapsed.get('#The_Dreg_Heap_col', True))                 else None
        expanded_tree_items[0].append([21, 0]) if (not collapsed.get('#The_Ringed_City_col', True))               else None

        # achievements
        expanded_tree_items[1].append([0,  0]) if (not collapsed.get('#Master_of_Expression_col', True))      else None
        expanded_tree_items[1].append([1,  0]) if (not collapsed.get('#Master_of_Sorceries_col', True))       else None
        expanded_tree_items[1].append([2,  0]) if (not collapsed.get('#Master_of_Pyromancies_col', True))     else None
        expanded_tree_items[1].append([3,  0]) if (not collapsed.get('#Master_of_Miracles_col', True))        else None
        expanded_tree_items[1].append([4,  0]) if (not collapsed.get('#DLC_Spells_col', True))                else None
        expanded_tree_items[1].append([5,  0]) if (not collapsed.get('#Master_of_Rings_col', True))           else None
        expanded_tree_items[1].append([6,  0]) if (not collapsed.get('#DLC_Rings_col', True))                 else None
        expanded_tree_items[1].append([7,  0]) if (not collapsed.get('#Master_of_Infusion_col', True))        else None
        expanded_tree_items[1].append([8,  0]) if (not collapsed.get('#Ending_Achievements_col', True))       else None
        expanded_tree_items[1].append([9,  0]) if (not collapsed.get('#Boss_Achievements_col', True))         else None
        expanded_tree_items[1].append([10, 0]) if (not collapsed.get('#Misc_Achievements_col', True))         else None
        expanded_tree_items[1].append([11, 0]) if (not collapsed.get('#Covenants_Achievements_col', True))    else None

        #weapons
        expanded_tree_items[2].append([0, 0]) if (not collapsed.get('#Weapons_col', True))    else None
        expanded_tree_items[2].append([1, 0]) if (not collapsed.get('#Shields_col', True))    else None

        #armor
        expanded_tree_items[3].append([0, 0]) if (not collapsed.get('#Helms_col', True))      else None
        expanded_tree_items[3].append([1, 0]) if (not collapsed.get('#Chests_col', True))     else None
        expanded_tree_items[3].append([2, 0]) if (not collapsed.get('#Gauntlets_col', True))  else None
        expanded_tree_items[3].append([3, 0]) if (not collapsed.get('#Leggings_col', True))   else None

        #misc
        # collapsed['#Pickle_Pee_Pump-a-Rum_Crow_col'] = False
        # collapsed['#Soul_Types_col'] = False
        return expanded_tree_items

    def getExpandedItemsAt(self, index) -> list:
        return self.expanded_tree_items[index]

    def getCurrentTab(self) -> int:
        return self._current_tab

    def setCurrentTab(self, i):
        self._current_tab = i

    def updateProfileContent(self):
        dumped_profile_data = ProfileDS3.dumpProfile(self)
        updated_profile = ProfileDS3.createProfile(FileData.getProfileName(self.filename), dumped_profile_data)

        self.playthrough_data       = updated_profile.playthrough_data
        self.achievements_data      = updated_profile.achievements_data
        self.weaponsandshields_data = updated_profile.weaponsandshields_data
        self.armor_data             = updated_profile.armor_data
        self.misc_data              = updated_profile.misc_data

        self.showCompleted   = updated_profile.showCompleted
        self.showBosses      = updated_profile.showBosses
        self.showNPCs        = updated_profile.showNPCs
        self.showEstusShards = updated_profile.showEstusShards
        self.showBones       = updated_profile.showBones
        self.showTomes       = updated_profile.showTomes
        self.showCoals       = updated_profile.showCoals
        self.showAshes       = updated_profile.showAshes
        self.showGestures    = updated_profile.showGestures
        self.showSorceries   = updated_profile.showSorceries
        self.showPyromancies = updated_profile.showPyromancies
        self.showMiracles    = updated_profile.showMiracles
        self.showRings       = updated_profile.showRings
        self.showWeapons     = updated_profile.showWeapons
        self.showArmor       = updated_profile.showArmor
        self.showTitanite    = updated_profile.showTitanite
        self.showGems        = updated_profile.showGems
        self.showCovenants   = updated_profile.showCovenants
        self.showMiscItems   = updated_profile.showMiscItems

        # self.pl_checking            = updated_profile.pl_checking
        self.journey                = updated_profile.journey
        self._current_tab           = updated_profile._current_tab
        self.expanded_tree_items    = updated_profile.expanded_tree_items
        self.show_urls_list         = updated_profile.show_urls_list

    @staticmethod
    def _isValidFile(filename) -> bool:
        if not os.path.exists(filename):
            Logger.LOGi(ProfileDS3.tag, "file '{}' not found", filename)
            return False

        with open(filename, 'r') as input_file:
            try:
                data = json.load(input_file)
                def isDictContains(dictionary:dict, *args):
                    all_args = True
                    for arg in args:
                        if dictionary.get(arg, None) is None:
                            Logger.LOGe(ProfileDS3.tag, "'{}' not found {}", FileData.getProfileName(filename), arg)
                            all_args = False
                    return all_args

                keys = ('Playthrough Checklists',
                        'Achievement Checklists',
                        'Weapons/Shields Checklists',
                        'Armor Checklists',
                        'Misc Checklists',
                        'expanded_tree_items',
                        'current_tab',
                        'filtering_dict',
                        'urlslist',
                        'journey')
                if not isDictContains(data, *keys):
                    return False

                return True

            except json.JSONDecodeError:
                return False

    @staticmethod
    def getUrlsFromString(string: str) -> tuple:
        urls_names = []
        urls = []

        splitted_by_href = string.split('<a href=')
        # print('splitted_by_href: ', *splitted_by_href, sep='\n\t')
        splitted_by_tag = []
        for i in splitted_by_href:
            nsplit = i.split('</a>')
            splitted_by_tag.append(nsplit[:-1] if nsplit[-1] == '' else nsplit)

        # print('\n\nsplitted_by_tag: ', *splitted_by_tag, sep='\n\t', end = '\n\n')
        for k in range(len(splitted_by_tag) - 1):
            for z in splitted_by_tag[k + 1]:
                a = z.split('>', 1)
                if len(a) == 1:
                    continue

                if not (a[0] in urls):
                    urls.append(a[0])
                    urls_names.append(a[1])
        return urls_names, urls

    @staticmethod
    def cutUrlsFromString(string: str) -> str:
        new_string = ''
        splitted_by_href = string.split('<a href=')
        # print('splitted_by_href: ', *splitted_by_href, sep='\n\t')
        splitted_by_tag = []
        for i in splitted_by_href:
            nsplit = i.split('</a>')
            splitted_by_tag.append(nsplit[:-1] if nsplit[-1] == '' else nsplit)

        # print('\n\nsplitted_by_tag: ', *splitted_by_tag, sep='\n\t', end = '\n\n')
        new_string += splitted_by_href[0]
        for k in range(len(splitted_by_tag) - 1):
            for z in splitted_by_tag[k + 1]:
                a = z.split('>', 1)
                if len(a) == 1:
                    new_string += a[0]
                    continue
                new_string += a[1]
        return new_string


    @staticmethod
    def _loadPlaythroughData(dictionary: dict) -> dict:
        new_dict = dict()
        for location_name, massive_of_units in dictionary.items():
            new_dict[location_name] = []
            for pl_unit_data in massive_of_units:
                unit = DataUnitPlaythrough(pl_unit_data)
                new_dict[location_name].append(unit)
        return new_dict

    @staticmethod
    def _createPlaythroughData(dictionary: dict) -> dict:
        new_dict = dict()
        for location_name, massive_of_units in dictionary.items():
            new_dict[location_name] = []
            for pl_unit_data in massive_of_units:
                unit = DataUnitPlaythrough()
                unit.setId(pl_unit_data[0])
                unit.setClasses(pl_unit_data[1])
                unit.setString(ProfileDS3.cutUrlsFromString(pl_unit_data[2]))
                unit.setHyperlinks(*ProfileDS3.getUrlsFromString(pl_unit_data[2]))
                unit.setChecked(False)
                new_dict[location_name].append(unit)
        return new_dict

    @staticmethod
    def _dumpPlaythroughData(dictionary: dict) -> dict:
        new_dict = dict()
        for location_name, units in dictionary.items():
            new_dict[location_name] = []
            for unit in units:
                new_dict[location_name].append(unit.getMassive())
        return new_dict


    @staticmethod
    def _loadAchievementData(dictionary: dict) -> dict:
        new_dict = dict()
        for ach_name, ach_value in dictionary.items():
            if ach_name == 'Master of Rings':
                new_dict[ach_name] = dict()
                for ring_pltype, rings_massive in ach_value.items():
                    new_dict[ach_name][ring_pltype] = []
                    for unit_data in rings_massive:
                        unit = DataUnit(unit_data)
                        new_dict[ach_name][ring_pltype].append(unit)
                continue
            new_dict[ach_name] = []
            for unit_data in ach_value:
                unit = DataUnit()
                unit.loadMassive(unit_data)
                new_dict[ach_name].append(unit)
        return new_dict

    @staticmethod
    def _createAchievementData(dictionary: dict) -> dict:
        new_dict = dict()
        for ach_name, ach_value in dictionary.items():
            if ach_name == 'Master of Rings':
                new_dict[ach_name] = dict()
                for ring_pltype, rings_massive in ach_value.items():
                    new_dict[ach_name][ring_pltype] = []
                    for unit_data in rings_massive:
                        unit = DataUnit()
                        unit.setId(unit_data[0])
                        unit.setChecked(False)

                        unit.setString(ProfileDS3.cutUrlsFromString(unit_data[1]))
                        unit.setHyperlinks(*ProfileDS3.getUrlsFromString(unit_data[1]))

                        new_dict[ach_name][ring_pltype].append(unit)
                continue
            new_dict[ach_name] = []
            for unit_data in ach_value:
                unit = DataUnit()
                unit.setId(unit_data[0])
                unit.setChecked(False)

                unit.setString(ProfileDS3.cutUrlsFromString(unit_data[1]))
                unit.setHyperlinks(*ProfileDS3.getUrlsFromString(unit_data[1]))

                new_dict[ach_name].append(unit)
        return new_dict

    @staticmethod
    def _dumpAchievementData(dictionary: dict) -> dict:
        new_dict = dict()
        for ach_name, ach_value in dictionary.items():
            if ach_name == 'Master of Rings':
                new_dict[ach_name] = dict()
                for ring_pltype, rings_massive in ach_value.items():
                    new_dict[ach_name][ring_pltype] = []
                    for unit in rings_massive:
                        new_dict[ach_name][ring_pltype].append(unit.getMassive())
                continue
            new_dict[ach_name] = []
            for unit in ach_value:
                new_dict[ach_name].append(unit.getMassive())
        return new_dict


    @staticmethod
    def _loadArmorData(dictionary: dict) -> dict:
        new_dict = dict()
        for armor_type, armors_massive in dictionary.items():
            new_dict[armor_type] = []
            for unit_data in armors_massive:
                unit = DataUnit(unit_data)
                new_dict[armor_type].append(unit)
        return new_dict

    @staticmethod
    def _createArmorData(dictionary: dict) -> dict:
        new_dict = dict()
        for armor_type, armors_massive in dictionary.items():
            new_dict[armor_type] = []
            for unit_data in armors_massive:
                unit = DataUnit()
                unit.setId(unit_data[0])
                unit.setChecked(False)

                unit.setString(ProfileDS3.cutUrlsFromString(unit_data[1]))
                unit.setHyperlinks(*ProfileDS3.getUrlsFromString(unit_data[1]))

                new_dict[armor_type].append(unit)
        return new_dict

    @staticmethod
    def _dumpArmorData(dictionary: dict) -> dict:
        new_dict = dict()
        for armor_type, armors_massive in dictionary.items():
            new_dict[armor_type] = []
            for unit in armors_massive:
                new_dict[armor_type].append(unit.getMassive())
        return new_dict


    @staticmethod
    def _loadWeaponsData(dictionary: dict) -> dict:
        new_dict = dict()
        for checklist_type, checklist_dict in dictionary.items():
            new_dict[checklist_type] = dict()
            for weapon_type, weapons_massive in checklist_dict.items():
                new_dict[checklist_type][weapon_type] = []
                for unit_data in weapons_massive:
                    unit = DataUnit(unit_data)
                    new_dict[checklist_type][weapon_type].append(unit)
        return new_dict

    @staticmethod
    def _createWeaponsData(dictionary: dict) -> dict:
        new_dict = dict()
        for checklist_type, checklist_dict in dictionary.items():
            new_dict[checklist_type] = dict()
            for weapon_type, weapons_massive in checklist_dict.items():
                new_dict[checklist_type][weapon_type] = []
                for unit_data in weapons_massive:
                    unit = DataUnit()
                    unit.setId(unit_data[0])
                    unit.setChecked(False)

                    unit.setString(ProfileDS3.cutUrlsFromString(unit_data[1]))
                    unit.setHyperlinks(*ProfileDS3.getUrlsFromString(unit_data[1]))

                    new_dict[checklist_type][weapon_type].append(unit)
        return new_dict

    @staticmethod
    def _dumpWeaponsData(dictionary: dict) -> dict:
        new_dict = dict()
        for checklist_type, checklist_dict in dictionary.items():
            new_dict[checklist_type] = dict()
            for weapon_type, weapons_massive in checklist_dict.items():
                new_dict[checklist_type][weapon_type] = []
                for unit in weapons_massive:
                    new_dict[checklist_type][weapon_type].append(unit.getMassive())
        return new_dict


    @staticmethod
    def _loadMiscData(massive: list) -> list:
        new_list = []
        for unit_data in massive:
            unit = DataUnit(unit_data)
            new_list.append(unit)
        return new_list

    @staticmethod
    def _createMiscData(massive: list) -> list:
        new_list = []
        for unit_data in massive:
            unit = DataUnit()
            unit.setId(unit_data[0])
            unit.setChecked(False)

            unit.setString(ProfileDS3.cutUrlsFromString(unit_data[1]))
            unit.setHyperlinks(*ProfileDS3.getUrlsFromString(unit_data[1]))

            new_list.append(unit)
        return new_list

    @staticmethod
    def _dumpMiscData(massive: list) -> list:
        new_list = []
        for unit in massive:
            new_list.append(unit.getMassive())
        return new_list


class DataUnit:
    def __init__(self, massive:list=None):
        self._id = None
        self._string  = None
        self._checked = False

        self._url_titles = []
        self._urls = []

        if not (massive is None):
            self.loadMassive(massive)

    def setHyperlinks(self, titles:list, urls:list):
        if len(urls) != len(titles):
            print('{} {} bad parsed links'.format(self._id, self._string))

        self._url_titles = titles
        self._urls = urls

    def getHyperlinks(self) -> "tuple of titles[0] and urls[1]":
        return self._url_titles, self._urls

    def id(self) -> str:
        return self._id

    def setId(self, ident:str):
        self._id = ident


    def string(self) -> str:
        return self._string

    def setString(self, ident:str):
        self._string = ident


    def checked(self) -> bool:
        return self._checked

    def setChecked(self, checked:bool):
        self._checked = checked

    def loadMassive(self, massive: list):
        self.setId(massive[0])
        self.setString(massive[1])
        self.setChecked(massive[2])
        self.setHyperlinks(*massive[3])

    def getMassive(self) -> list:
        return [self.id(), self.string(), self.checked(), self.getHyperlinks()]


class DataUnitPlaythrough(DataUnit):
    def __init__(self, massive:list=None):
        DataUnit.__init__(self)
        self._classes = []

        if not (massive is None):
            self.loadMassive(massive)

    def classes(self) -> list:
        return self._classes

    def setClasses(self, classes:str):
        self._classes.clear()
        for cls in classes.split(' '):
            self._classes.append(cls)

    def loadMassive(self, massive: list):
        self.setId(massive[0])
        self.setClasses(massive[1])
        self.setString(massive[2])
        self.setChecked(massive[3])
        self.setHyperlinks(*massive[4])

    def getMassive(self) -> list:
        return [self.id(), ' '.join(self.classes()), self.string(), self.checked(), self.getHyperlinks()]
