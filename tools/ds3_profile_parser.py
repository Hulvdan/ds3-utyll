# Dark Souls 3 Cheat Sheet tool
# Copyright (C) 2018  Aleksey Chistov
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# Source code at https://bitbucket.org/Hulvdan/ds3-utyll

def getManifestTime() -> str:
    from time import gmtime, strftime
    _format_string = '%H:%M:%S - %d.%m.%Y'
    return strftime(_format_string, gmtime())

def processLine(input_string:str) -> str:
    input_string = input_string[:-1]
    input_string = input_string.replace('</span>              ', '</span>').replace('<span class="p"> + </span>', ' + ')
    input_string = input_string.replace('Г¤', 'a')
    splitted = input_string.split('&nbsp;')
    new_string = splitted[0]
    if len(splitted) > 1:
        for string in splitted[1:]:
            if string[0] == 'x':
                if string[1].isdecimal():
                    if string[2].isdecimal():
                        number = string[:3]
                        new_string += ' (' + number + ')' + string[3:]
                        continue
                    number = string[:2]
                    new_string += ' (' + number + ')' + string[2:]

    return new_string

def getBigDataFromFileLines(import_file_lines) -> dict:
    min_app_version = '1.2.2'
    big_data = {
        'manifest': getManifestTime(),
	    "min_app_version" : "1.2.2",
        'Playthrough Checklists': dict(),
        'Achievement Checklists': dict(),
        'Weapons/Shields Checklists': dict(),
        'Armor Checklists': dict(),
        'Misc Checklists': list()
    }

    stage = 0
    c_stage_started = 0
    c_stage_playthrough = 1
    c_stage_achievements = 2
    c_stage_weapons = 3
    c_stage_armor = 4
    c_stage_misc = 5
    c_stage_end = 6

    stage_data = 0
    c_stage_data_acquiring = 1
    c_stage_data_acquiring_alt_header = 2
    c_stage_data_acquiring_alt_data = 3

    header = ''
    header_alt = ''
    for line in import_file_lines:
        line = processLine(line)
        if stage == c_stage_started:
            if 'playthrough_list' in line:
                stage = c_stage_playthrough
                continue

        if stage == c_stage_playthrough:
            if stage_data == c_stage_data_acquiring:
                if '<li data-id="' in line:
                    identifier = line.split('<li data-id="', 1)[-1].split('" class="', 1)[0]
                    data_class = line.split('<li data-id="', 1)[-1].split('" class="', 1)[1].split('">', 1)[0]
                    string = line.split('<li data-id="', 1)[-1].split('" class="', 1)[1].split('">', 1)[1].rsplit('</li>', 1)[0]

                    big_data['Playthrough Checklists'][header].append([identifier, data_class, string])
                    continue

                if '</ul>' in line:
                    stage_data = 0
                    continue
                continue


            if '<h3 id="' in line:
                header = line.split('<h3 id="', 1)[-1].split('">', 1)[0].replace('_', ' ')
                big_data['Playthrough Checklists'][header] = list()
                stage_data = c_stage_data_acquiring
                continue

            if '</div>' in line:
                stage = c_stage_achievements
                stage_data = -2
                continue
            continue


        if stage == c_stage_achievements:
            if stage_data == c_stage_data_acquiring:
                if '<li data-id="' in line:
                    identifier = line.split('<li data-id="', 1)[-1].split('">', 1)[0]
                    string = line.split('<li data-id="', 1)[-1].split('">', 1)[-1].rsplit('</li>', 1)[0].replace('<b>', '').replace('</b>', '').replace('<strong>', '').replace('</strong>', '')
                    big_data['Achievement Checklists'][header].append([identifier, string])
                    continue

                if '</ul>' in line:
                    stage_data = 0
                    continue
                continue

            if stage_data == c_stage_data_acquiring_alt_header:
                if '<h4 id="' in line:
                    header_alt = line.rsplit('</h4>', 1)[0].rsplit('">')[-1]
                    big_data['Achievement Checklists'][header][header_alt] = []
                    stage_data = c_stage_data_acquiring_alt_data
                    continue

                if '</div>' in line:
                    stage_data = 0
                    continue
                continue

            if stage_data == c_stage_data_acquiring_alt_data:
                if '<li data-id="' in line:
                    identifier = line.split('<li data-id="', 1)[-1].split('">', 1)[0]
                    string = line.split('<li data-id="', 1)[-1].split('">', 1)[-1].rsplit('</li>', 1)[0].replace('<b>', '').replace('</b>', '').replace('<strong>', '').replace('</strong>', '')
                    big_data['Achievement Checklists'][header][header_alt].append([identifier, string])
                    continue

                if '</ul>' in line:
                    stage_data = c_stage_data_acquiring_alt_header
                    continue
                continue


            if '<h3 id="' in line:
                header = line.lstrip('<h3 id="').split('"><', 1)[0].replace('_', ' ')
                if header == 'Master of Rings':
                    big_data['Achievement Checklists'][header] = {}
                    stage_data = c_stage_data_acquiring_alt_header
                    continue
                big_data['Achievement Checklists'][header] = []
                stage_data = c_stage_data_acquiring
                continue

            if '</div>' in line:
                if stage_data < 0:
                    stage_data += 1
                    continue
                stage = c_stage_weapons
                stage_data = -2
                continue
            continue

        if stage == c_stage_weapons:
            if '<h3 id="' in line:
                l = line.split('<h3 id="', 1)[-1].split('">', 1)[0]
                if l == 'Weapons':
                    header_alt = 'Weapon Checklists'
                elif l == "Shields":
                    header_alt = 'Shield Checklists'
                continue

            if stage_data == c_stage_data_acquiring:
                if '<li data-id="' in line:
                    identifier = line.split('<li data-id="', 1)[-1].split('">', 1)[0]
                    string = line.split('<li data-id="', 1)[-1].split('">', 1)[-1].rsplit('</li>', 1)[0]
                    big_data["Weapons/Shields Checklists"][header_alt][header].append([identifier, string])
                    continue

                if '</ul>' in line:
                    stage_data = -1
                    continue


            if '<h4>' in line:
                header = line.split('</h4>', 1)[0].split('<h4>', 1)[-1].split('">', 1)[-1].split('</a>', 1)[0]
                big_data["Weapons/Shields Checklists"][header_alt][header] = []
                stage_data = c_stage_data_acquiring
                continue


            if '<div id="weapons_list">' in line:
                big_data["Weapons/Shields Checklists"]['Weapon Checklists'] = {}
                big_data["Weapons/Shields Checklists"]['Shield Checklists'] = {}
                continue

            if '</div>' in line:
                if stage_data < 0:
                    stage_data += 1
                    continue
                stage = c_stage_armor
                stage_data = -2
                continue
            continue

        if stage == c_stage_armor:
            if '<div id="armors_list">' in line:
                stage_data = c_stage_data_acquiring
                continue

            if stage_data == c_stage_data_acquiring:
                if '<h3 id="' in line:
                    header = line.split('<h3 id="', 1)[-1].split('">', 1)[0]
                    big_data['Armor Checklists'][header] = list()
                    stage_data = c_stage_data_acquiring_alt_data
                    continue

                if '</div>' in line:
                    stage_data = 0
                    continue
                continue

            if stage_data == c_stage_data_acquiring_alt_data:
                if '<li data-id="' in line:
                    identifier = line.split('<li data-id="', 1)[-1].split('">', 1)[0]
                    string = line.split('<li data-id="', 1)[-1].split('">', 1)[1].rsplit('</li>', 1)[0]
                    big_data['Armor Checklists'][header].append([identifier, string])
                    continue
                if '</ul>' in line:
                    stage_data = c_stage_data_acquiring
                    continue
                continue

            if '</div>' in line:
                if stage_data < 0:
                    stage_data += 1
                    continue
                stage = c_stage_misc
                stage_data = -1
                continue
            continue

        if stage == c_stage_misc:
            if stage_data == c_stage_data_acquiring:
                if '<li data-id="' in line:
                    identifier = line.split('<li data-id="', 1)[-1].split('">', 1)[0]
                    string = line.split('<li data-id="', 1)[-1].split('">', 1)[-1].rsplit('</li>', 1)[0]
                    big_data['Misc Checklists'].append([identifier, string])
                    continue

                if '</ul>' in line:
                    break
                    # stage = c_stage_end
                    # stage_data = 0
                    # continue
                continue

            if '<h3 id="' in line:
                stage_data = c_stage_data_acquiring
                continue

        # if stage == c_stage_end:
        #     continue
    return big_data


def main():
    import urllib.error
    import urllib.request
    import os
	
    import_filename = 'index.html'
    export_filename = os.path.normpath(r'..\assets\ds3_profile.json')
    import_filename_link = 'https://raw.githubusercontent.com/ZKjellberg/dark-souls-3-cheat-sheet/gh-pages/index.html'

    copyright_string = """Dark Souls 3 Cheat Sheet tool\nCopyright (C) 2018  Aleksey Chistov.\nLicense - GNU GPLv3\n"""
    print(copyright_string)

    try:
        print("Downloading '{}' to '{}'".format(import_filename_link, import_filename))
        urllib.request.urlretrieve(import_filename_link, filename=import_filename)
        with open(import_filename) as import_file:
            lines = import_file.readlines()

        print('Creating big_data')
        big_data = getBigDataFromFileLines(lines)
        import json
        with open(export_filename, 'w') as export_file:
            print('Dumping big_data')
            json.dump(big_data, export_file, indent='\t')
            print('Success')

    except urllib.error.URLError as error:
        print("Error opening URL '{}'. Exception: '{}'".format(import_filename_link, error))

    if os.path.exists(import_filename):
        print("Deleting '{}'".format(import_filename))
        os.remove(import_filename)

    input('\nPress Enter to exit. ')

if __name__ == '__main__':
    main()
