

def isVersionBiggerOrEqualThan(v1:str, v2:str):
    # returns true if v1 >= v2
    v1_numbers = [int(i) for i in v1.split('.')]
    v2_numbers = [int(i) for i in v2.split('.')]

    while len(v1_numbers) < 3:
        v1_numbers.append(0)
    while len(v2_numbers) < 3:
        v2_numbers.append(0)

    for i in range(3):
        if v1_numbers[i] < v2_numbers[i]:
            return False

    return True
