# Dark Souls 3 Cheat Sheet tool
# Copyright (C) 2018  Aleksey Chistov
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# Source code at https://bitbucket.org/Hulvdan/ds3-utyll

from PyQt5 import QtCore, QtWidgets, QtGui
from Logger import *


class UrlListView(QtWidgets.QListView):
    """Menu (primarily of urls) shown in bottom right corner of trees"""
    tag = 'UrlListView'
    _selection_mode = 0
    def __init__(self, parent=None):
        QtWidgets.QListView.__init__(self, parent)
        self.setObjectName(__class__.tag)

        self.installEventFilter(self)
        self.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)

    def clear(self):
        self.clearSelection()
        self.model().clear()

    def focusInEvent(self, *args, **kwargs):
        # Logger.LOGd(UrlListView.tag, 'focusInEvent')
        super().focusInEvent(*args, **kwargs)
        if len(self.selectedIndexes()) == 0:
            selection_model = self.selectionModel()
            index = self.model().index(0, 0)
            if index.isValid():
                selection_model.select(index, QtCore.QItemSelectionModel.Select)
                selection_model.setCurrentIndex(index, QtCore.QItemSelectionModel.Select)

    def focusOutEvent(self, *args, **kwargs):
        # Logger.LOGd(UrlListView.tag, 'focusOutEvent')
        super().focusOutEvent(*args, **kwargs)
        self.selectionModel().clearSelection()
        self.parent().setFocus()

    def setFocus(self, *args):
        # Logger.LOGd(UrlListView.tag, 'setFocus')
        super().setFocus(*args)

    def eventFilter(self, obj, event):
        if event.type() == QtCore.QEvent.KeyPress:
            if self.hasFocus():
                key = event.key()
                if QtCore.Qt.Key_Escape == key:
                    self.clearFocus()
                    self.parent().setFocus()
                    return True

                if __class__._selection_mode == 0:
                    if QtCore.Qt.Key_Up == key:
                        if self.selectionModel().currentIndex() == self.model().index(0,0):
                            self.clearFocus()
                            self.parent().setFocus()
                            return True

                    if QtCore.Qt.Key_Down == key:
                        if self.selectionModel().currentIndex() == self.model().index(self.model().rowCount()-1, 0):
                            self.clearFocus()
                            self.parent().setFocus()
                            return True

                if __class__._selection_mode == 1:
                    if QtCore.Qt.Key_Up == key:
                        if self.selectionModel().currentIndex() == self.model().index(0,0):
                            self.selectionModel().clearSelection()
                            self.selectionModel().setCurrentIndex(self.model().index(self.model().rowCount()-1, 0),
                                                                  QtCore.QItemSelectionModel.Select)
                            return True

                    if QtCore.Qt.Key_Down == key:
                        if self.selectionModel().currentIndex() == self.model().index(self.model().rowCount()-1, 0):
                            self.selectionModel().clearSelection()
                            self.selectionModel().setCurrentIndex(self.model().index(0, 0),
                                                                  QtCore.QItemSelectionModel.Select)
                            return True

        return super().eventFilter(obj, event)


