# Dark Souls 3 Cheat Sheet tool
# Copyright (C) 2018  Aleksey Chistov
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# Source code at https://bitbucket.org/Hulvdan/ds3-utyll

def main():
    import sys
    from PyQt5 import QtGui
    from PyQt5 import QtWidgets
    from Logger import Logger
    from GUI import GUI
    import FileData

    Logger.setLogFile('log.txt')
    app = QtWidgets.QApplication(sys.argv)
    app.setWindowIcon(QtGui.QIcon(FileData.icon_path))
    window = GUI()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
