# Dark Souls 3 Cheat Sheet tool
# Copyright (C) 2018  Aleksey Chistov
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# Source code at https://bitbucket.org/Hulvdan/ds3-utyll

import os
import json


ext_profile = 'json'
ext_config = 'json'
ext_export = 'json'
ext_theme = 'qss'
ext_theme_config = 'qfg'
ext_gui = 'ui'

default_profile = 'Default Profile'
default_theme = 'default'

export_filename = 'export'
data_armor = os.path.normpath('assets\\ds3_armor_sets.json')
ds3_data_profile = os.path.normpath('assets\\ds3_profile')
data_config = '_config'

dialogNPNone = 'None'
menuSTNew = 'New Profile'
asset_folder = 'assets'
profile_folder = 'profiles'

remote_profile_filename = 'index.html'
remote_profile_link = 'https://bitbucket.org/Hulvdan/ds3-utyll/raw/master/assets/ds3_profile.json'

def isValidFilename(filename:str) -> bool:
    for char in filename:
        if char in isValidFilename.illegal_characters:
            return False
    return True
isValidFilename.illegal_characters = r'\/:*?"<>|'


def getProfiles() -> list:
    """Returns list of [0] profile names list, [1] profile filenames list"""
    profiles = []
    profile_folder_path = profile_folder

    if not os.path.exists(profile_folder_path):
        os.mkdir(profile_folder_path)

    directory = os.listdir(profile_folder_path)
    for i in range(len(directory)):
        splitted = directory[i].rsplit('.', 1)
        if not len(splitted) < 2 or splitted[-1] != ext_profile:
            profile_path = os.path.join(profile_folder_path, directory[i])
            profile_name = splitted[0].rsplit(os.path.sep, 1)[-1]
            profiles.append((profile_name, profile_path))

    return profiles

def getProfileDataFilename() -> str:
    return ds3_data_profile + '.' + ext_profile

def getNewProfileDataFilename() -> str:
    files = os.listdir(os.path.abspath(''))
    max_num = 0
    for data_file in filter(lambda x: (ds3_data_profile in x) and ('.' + ext_profile in x), files):
        num = data_file.rsplit('_', 1)[-1].rsplit('.', 1)[0]
        if num.isdigit():
            max_num = int(num)

    return ds3_data_profile + '_' + str(max_num + 1) + '.' + ext_profile

def fileConfig(config_name) -> str:
    return os.path.join(config_name + '.' + ext_config)

def fileTheme(theme_name) -> str:
    return os.path.join(asset_folder, theme_name + '.' + ext_theme)

def fileProfile(profile_name) -> str:
    return os.path.join(profile_folder, profile_name) + '.' + ext_profile

def fileGui(gui_name) -> str:
    return os.path.join(asset_folder, gui_name) + '.' + ext_gui

def getProfileName(profile_path:str) -> str:
    return profile_path.rsplit('.', 1)[0].rsplit(os.path.sep)[-1]

def fileConfigDefault() -> str:
    return fileConfig(data_config)

def fileThemeDefault() -> str:
    return fileTheme(default_theme)

def fileProfileDefault() -> str:
    return fileProfile(default_profile)


filename_gui = fileGui('gui')
filename_dialog_new_profile = fileGui('gui_new_profile')
filename_dialog_about = fileGui('gui_about')
filename_dialog_export = fileGui('gui_export')
filename_dialog_import = fileGui('gui_import')
filename_dialog_rename = fileGui('gui_rename')
filename_dialog_message = fileGui('gui_message')
filename_dialog_confirm = fileGui('gui_confirm')
icon_path = os.path.join(asset_folder, 'icon.ico')

exe_filename = 'DS3 Progress'
app_version = '1.2.4'
config_manifest = '12:06 - 16 feb 2018'
profile_manifest = ''
if os.path.exists(ds3_data_profile + '.' + ext_profile):
    with open(os.path.join(ds3_data_profile + '.' + ext_profile)) as big_data:
        profile_manifest = json.load(big_data)['manifest']
