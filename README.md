# **Dark Souls 3** Cheat Sheet Tool by **Hulvdan**

---

## **It supports**
- ### Multiple profiles
- ### Import/export from **ZKjellberg**'s [browser version](https://zkjellberg.github.io/dark-souls-3-cheat-sheet/)
- ### Profile updating

---

## **Releases**
* ### [**1.2.4**](https://www.dropbox.com/s/xzjk9ytiaq48foq/ds3-utyll%20v1.2.4.zip?dl=0)
    * #### Fixed issue with parent checkboxes which didn't work well
* ### [**1.2.3**](https://www.dropbox.com/s/e4fpg1ufyv0cxjt/ds3-utyll%20v1.2.3.zip?dl=0)
    * #### Collapse/Expand All
	* #### Applied all **loidas's** changes for browser version for program to be compatible
* ### [**1.2.2**](https://www.dropbox.com/s/jqngh33jik6gaze/ds3-utyll%20v1.2.2.zip?dl=0)
    * #### Option to **Check Armor Sets**
    * #### Added list of urls to bottom right corner that can be optionally hidden
    * #### Normal checkboxes
* ### [**1.2.1**](https://www.dropbox.com/s/4yf8peiphbmvxvr/ds3-utyll%20v1.2.1.zip?dl=0)
    * #### **Fixed Crash** on update profile message dialog
    * #### Fixed message dialog width
    * #### Playthrough filters highlighting
    * #### Changed text color
* ### [**1.2**](https://www.dropbox.com/s/gszukk89h139z2f/ds3-utyll%20v1.2.zip?dl=0)
    * #### Profile deleting confirmation dialog
    * #### Import/export from/to clipboard
    * #### Default profile now called 'Default Profile' unlike 'default' to avoid default profiles duplication in browser version
* ### [**1.1**](https://www.dropbox.com/s/g4u2nrksgr0on2q/ds3-utyll%20v1.1.zip?dl=0)
    * #### Manual option to update a profile content file (now it is 'ds3_profile.json' in 'assets' folder)
    * #### Fixed logging
* ### [**1.0**](https://www.dropbox.com/s/tevxed82deflgul/ds3-utyll%20v1.0.zip?dl=0)

---

## **Based on**
* ### [Python 3.6.4](https://www.python.org/)
* ### [PyQt5](https://www.riverbankcomputing.com/software/pyqt/intro)
* ### [PyInstaller](http://www.pyinstaller.org/)
* ### [**ZKjellberg**'s DS3 Cheat Sheet](https://zkjellberg.github.io/dark-souls-3-cheat-sheet/)

---

## **Requirements**
* ### [Universal C Runtime](https://support.microsoft.com/en-us/help/2999226/update-for-universal-c-runtime-in-windows) (Already in Win10)

---

## **License**
* ### [GNU GPLv3](https://bitbucket.org/Hulvdan/ds3-utyll/raw/master/LICENSE.txt)
