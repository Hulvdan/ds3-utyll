# Dark Souls 3 Cheat Sheet tool
# Copyright (C) 2018  Aleksey Chistov
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# Source code at https://bitbucket.org/Hulvdan/ds3-utyll

from DelegateUniversalTree import *


class DelegateTreePlaythrough(DelegateUniversalTree):
    _item_dict_string_key = 'string'

    def _initConnections(self):
        super()._initConnections()
        self._gui.actionShowCompleted.triggered.connect(self.onToggleCheckCompleted)

        self._gui.checkQuests.stateChanged.connect(self.onToggleCheckQuests)
        self._gui.checkUpgrades.stateChanged.connect(self.onToggleCheckUpgrades)
        self._gui.checkAchievements.stateChanged.connect(self.onToggleCheckAchievements)
        self._gui.checkGear.stateChanged.connect(self.onToggleCheckGear)
        self._gui.checkMaterials.stateChanged.connect(self.onToggleCheckMaterials)
        self._gui.checkOthers.stateChanged.connect(self.onToggleCheckOthers)
        self._gui.new_game_mode_radio.toggled.connect(
            lambda: self.onToggleGameMode() if self._gui.new_game_mode_radio.isChecked() else None)
        self._gui.new_game_mode_plus_radio.toggled.connect(
            lambda: self.onToggleGameMode() if self._gui.new_game_mode_plus_radio.isChecked() else None
        )
        self._gui.new_game_mode_plus_plus_radio.toggled.connect(
            lambda: self.onToggleGameMode() if self._gui.new_game_mode_plus_plus_radio.isChecked() else None
        )

    def _isItemNeedHiding(self, item):
        massive = self._goals_pairs[id(item)].classes()

        showBosses      = self._gui.checkBoss.checkState() == 2
        showNPCs        = self._gui.checkNPC.checkState() == 2
        showEstusShards = self._gui.checkEstus.checkState() == 2
        showBones       = self._gui.checkBone.checkState() == 2
        showTomes       = self._gui.checkTome.checkState() == 2
        showCoals       = self._gui.checkCoal.checkState() == 2
        showAshes       = self._gui.checkAsh.checkState() == 2
        showGestures    = self._gui.checkGesture.checkState() == 2
        showSorceries   = self._gui.checkSorcery.checkState() == 2
        showPyromancies = self._gui.checkPyromancy.checkState() == 2
        showMiracles    = self._gui.checkMiracle.checkState() == 2
        showRings       = self._gui.checkRing.checkState() == 2
        showWeapons     = self._gui.checkWeapon.checkState() == 2
        showArmor       = self._gui.checkArmor.checkState() == 2
        showTitanite    = self._gui.checkTitanite.checkState() == 2
        showGems        = self._gui.checkGem.checkState() == 2
        showCovenants   = self._gui.checkCovenant.checkState() == 2
        showMiscItems   = self._gui.checkMisc.checkState() == 2
        showNGP         = self._gui.new_game_mode_plus_radio.isChecked()
        showNGPP        = self._gui.new_game_mode_plus_plus_radio.isChecked()

        classBosses      = "f_boss"  in massive
        classNPCs        = "f_npc"   in massive
        classEstusShards = "f_estus" in massive
        classBones       = "f_bone"  in massive
        classTomes       = "f_tome"  in massive
        classCoals       = "f_coal"  in massive
        classAshes       = "f_ash"   in massive
        classGestures    = "f_gest"  in massive
        classSorceries   = "f_sorc"  in massive
        classPyromancies = "f_pyro"  in massive
        classMiracles    = "f_mirac" in massive
        classRings       = "f_ring"  in massive
        classWeapons     = "f_weap"  in massive
        classArmor       = "f_arm"   in massive
        classTitanite    = "f_tit"   in massive
        classGems        = "f_gem"   in massive
        classCovenants   = "f_cov"   in massive
        classMiscItems   = "f_misc"  in massive
        classHideInNGP   = "h_ng+"   in massive
        classNGP         = "s_ng+"   in massive
        classNGPP        = "s_ng++"  in massive

        boss       = classBosses      & showBosses
        npcs       = classNPCs        & showNPCs
        estusShard = classEstusShards & showEstusShards
        bone       = classBones       & showBones
        tome       = classTomes       & showTomes
        coal       = classCoals       & showCoals
        ash        = classAshes       & showAshes
        gesture    = classGestures    & showGestures
        sorcery    = classSorceries   & showSorceries
        pyromancy  = classPyromancies & showPyromancies
        miracle    = classMiracles    & showMiracles
        ring       = classRings       & showRings
        weapon     = classWeapons     & showWeapons
        armor      = classArmor       & showArmor
        titanite   = classTitanite    & showTitanite
        gem        = classGems        & showGems
        covenant   = classCovenants   & showCovenants
        misc       = classMiscItems   & showMiscItems

        if classHideInNGP and (showNGP or showNGPP):
            return True

        show = (boss or npcs or estusShard or bone or tome or coal or ash or gesture or sorcery or pyromancy
                or miracle or ring or weapon or armor or titanite or gem or covenant or misc)
        if classNGP:
            show &= showNGP or showNGPP
        elif classNGPP:
            show &= showNGPP
        return not show

    _updating = False
    def update(self):
        if not __class__._updating:
            super().update()

    def onToggleCheckQuests(self):
        if self._gui.checkQuests.isStateHandlingSupressed():
            return
        state = self._gui.checkQuests.checkState()
        __class__._updating = True
        self._gui.checkBoss.setCheckState(state)
        self._gui.checkNPC.setCheckState(state)
        self.onToggleCheckBoss()
        self.onToggleCheckNPC()
        __class__._updating = False
        self.update()

    def onToggleCheckUpgrades(self):
        if self._gui.checkUpgrades.isStateHandlingSupressed():
            return
        state = self._gui.checkUpgrades.checkState()
        __class__._updating = True
        self._gui.checkEstus.setCheckState(state)
        self._gui.checkBone.setCheckState(state)
        self._gui.checkTome.setCheckState(state)
        self._gui.checkCoal.setCheckState(state)
        self._gui.checkAsh.setCheckState(state)
        self.onToggleCheckEstus()
        self.onToggleCheckBone()
        self.onToggleCheckTome()
        self.onToggleCheckCoal()
        self.onToggleCheckAsh()
        __class__._updating = False
        self.update()

    def onToggleCheckAchievements(self):
        if self._gui.checkAchievements.isStateHandlingSupressed():
            return
        state = self._gui.checkAchievements.checkState()
        __class__._updating = True
        self._gui.checkGesture.setCheckState(state)
        self._gui.checkSorcery.setCheckState(state)
        self._gui.checkPyromancy.setCheckState(state)
        self._gui.checkMiracle.setCheckState(state)
        self._gui.checkRing.setCheckState(state)
        self.onToggleCheckGesture()
        self.onToggleCheckSorcery()
        self.onToggleCheckPyromancy()
        self.onToggleCheckMiracle()
        self.onToggleCheckRing()
        __class__._updating = False
        self.update()

    def onToggleCheckGear(self):
        if self._gui.checkGear.isStateHandlingSupressed():
            return
        state = self._gui.checkGear.checkState()
        __class__._updating = True
        self._gui.checkWeapon.setCheckState(state)
        self._gui.checkArmor.setCheckState(state)
        self.onToggleCheckWeapon()
        self.onToggleCheckArmor()
        __class__._updating = False
        self.update()

    def onToggleCheckMaterials(self):
        if self._gui.checkMaterials.isStateHandlingSupressed():
            return
        state = self._gui.checkMaterials.checkState()
        __class__._updating = True
        self._gui.checkTitanite.setCheckState(state)
        self._gui.checkGem.setCheckState(state)
        self.onToggleCheckTitanite()
        self.onToggleCheckGem()
        __class__._updating = False
        self.update()

    def onToggleCheckOthers(self):
        if self._gui.checkOthers.isStateHandlingSupressed():
            return
        state = self._gui.checkOthers.checkState()
        __class__._updating = True
        self._gui.checkCovenant.setCheckState(state)
        self._gui.checkMisc.setCheckState(state)
        self.onToggleCheckCovenant()
        self.onToggleCheckMisc()
        __class__._updating = False
        self.update()


    def onToggleCheckCompleted(self):
        self._gui.profile.showCompleted = self._gui.actionShowCompleted.isChecked()
        self.update()

    def onToggleCheckBoss(self):
        self._gui.profile.showBosses = bool(self._gui.checkBoss.checkState())
        self.update()

    def onToggleCheckNPC(self):
        self._gui.profile.showNPCs = bool(self._gui.checkNPC.checkState())
        self.update()

    def onToggleCheckEstus(self):
        self._gui.profile.showEstusShards = bool(self._gui.checkEstus.checkState())
        self.update()

    def onToggleCheckBone(self):
        self._gui.profile.showBones = bool(self._gui.checkBone.checkState())
        self.update()

    def onToggleCheckTome(self):
        self._gui.profile.showTomes = bool(self._gui.checkTome.checkState())
        self.update()

    def onToggleCheckCoal(self):
        self._gui.profile.showCoals = bool(self._gui.checkCoal.checkState())
        self.update()

    def onToggleCheckAsh(self):
        self._gui.profile.showAshes = bool(self._gui.checkAsh.checkState())
        self.update()

    def onToggleCheckGesture(self):
        self._gui.profile.showGestures = bool(self._gui.checkGesture.checkState())
        self.update()

    def onToggleCheckSorcery(self):
        self._gui.profile.showSorceries = bool(self._gui.checkSorcery.checkState())
        self.update()

    def onToggleCheckPyromancy(self):
        self._gui.profile.showPyromancies = bool(self._gui.checkPyromancy.checkState())
        self.update()

    def onToggleCheckMiracle(self):
        self._gui.profile.showMiracles = bool(self._gui.checkMiracle.checkState())
        self.update()

    def onToggleCheckRing(self):
        self._gui.profile.showRings = bool(self._gui.checkRing.checkState())
        self.update()

    def onToggleCheckWeapon(self):
        self._gui.profile.showWeapons = bool(self._gui.checkWeapon.checkState())
        self.update()

    def onToggleCheckArmor(self):
        self._gui.profile.showArmor = bool(self._gui.checkArmor.checkState())
        self.update()

    def onToggleCheckTitanite(self):
        self._gui.profile.showTitanite = bool(self._gui.checkTitanite.checkState())
        self.update()

    def onToggleCheckGem(self):
        self._gui.profile.showGems = bool(self._gui.checkGem.checkState())
        self.update()

    def onToggleCheckCovenant(self):
        self._gui.profile.showCovenants = bool(self._gui.checkCovenant.checkState())
        self.update()

    def onToggleCheckMisc(self):
        self._gui.profile.showMiscItems = bool(self._gui.checkMisc.checkState())
        self.update()

    def onToggleGameMode(self):
        if self._gui.new_game_mode_radio.isChecked():
            self._gui.profile.journey = 1
        elif self._gui.new_game_mode_plus_radio.isChecked():
            self._gui.profile.journey = 2
        elif self._gui.new_game_mode_plus_plus_radio.isChecked():
            self._gui.profile.journey = 3
        self.update()
