# Dark Souls 3 Cheat Sheet tool
# Copyright (C) 2018  Aleksey Chistov
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# Source code at https://bitbucket.org/Hulvdan/ds3-utyll

class RetimerCallback:
    """Class used to call functions after period of time in pair with QTimer
    QTimer duration must be lesser in 10 times than RetimerCallback duration"""
    def __init__(self, time, callback):
        self._duration = time
        self._dt = time / 10
        self._elapsed = 0
        self._callback = callback
        self._ended = True

    def refresh(self):
        """Timer will start from beginning to prevent callback calling
        or call one more time after timer duration"""
        self._ended = False
        self._elapsed = 0

    def update(self, dt):
        """This method should be called by QTimer
        dt is timer interval"""
        if not self._ended:
            self._elapsed += dt
            if self._elapsed >= self._duration:
                if not (self._callback is None):
                    self._callback()
                self._elapsed = 0
                self._ended = True
