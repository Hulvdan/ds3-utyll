# Dark Souls 3 Cheat Sheet tool
# Copyright (C) 2018  Aleksey Chistov
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# Source code at https://bitbucket.org/Hulvdan/ds3-utyll

from PyQt5 import QtCore, QtGui, QtWidgets, uic
from ProfileDS3 import *
from DialogMessage import *
import FileData
import os


class DialogNewProfile(QtWidgets.QDialog):
    def __init__(self, parent, flags):
        QtWidgets.QDialog.__init__(self, parent, flags)
        uic.loadUi(FileData.filename_dialog_new_profile, self)
        self._loadFilesInComboBox()
        self.pushButtonOK.released.connect(self.accept)
        self.pushButtonCancel.released.connect(self.reject)

    def _loadFilesInComboBox(self):
        self.comboBox.clear()
        self.comboBox.addItem(FileData.dialogNPNone)

        directory = os.listdir(FileData.profile_folder)
        for i in range(len(directory)):
            splitted = directory[i].rsplit('.', 1)

            if (not len(splitted) < 2) or splitted[-1] != FileData.ext_profile:
                self.comboBox.addItem(splitted[0].rsplit(os.path.sep, 1)[0])

    @staticmethod
    def execute(parent=None) -> ProfileDS3:
        dialog = DialogNewProfile(parent, QtCore.Qt.WindowCloseButtonHint)
        result = dialog.exec_()

        if result == 0:
            return None

        name = dialog.lineEdit.text()
        current = dialog.comboBox.currentText()
        profile = dialog._createProfile(name, current)
        dialog.deleteLater()

        return profile

    @staticmethod
    def _createProfile(name, import_name):
        if name == '':
            return None

        if not FileData.isValidFilename(name):
            DialogMessage.execute("'{}' filename is invalid".format(name), True)
            return None


        if import_name == FileData.dialogNPNone:
            return ProfileDS3(FileData.fileProfile(name), isNew=True)
        else:
            profile = ProfileDS3(FileData.fileProfile(import_name))
            profile.filename = FileData.fileProfile(name)
            return profile

