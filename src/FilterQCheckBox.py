# Dark Souls 3 Cheat Sheet tool
# Copyright (C) 2018  Aleksey Chistov
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# Source code at https://bitbucket.org/Hulvdan/ds3-utyll

from PyQt5 import QtCore, QtWidgets, QtGui, Qt
from FilterItemsListView import *
from Logger import *


class FilterQCheckBox(QtWidgets.QCheckBox):
    tag = 'FilterQCheckBox'
    def __init__(self, child_list:FilterItemsListView, parent=None):
        QtWidgets.QCheckBox.__init__(self, parent)
        self.setObjectName(__class__.tag)
        self._child_list = child_list
        # self._child_list.setFocusPolicy(QtCore.Qt.TabFocus)
        self.installEventFilter(self)
        self._state_handling_supressed = False

    def setStateHandlingSupressed(self, supress:bool):
        self._state_handling_supressed = supress

    def isStateHandlingSupressed(self):
        return self._state_handling_supressed

    def updateState(self):
        all_checked = True
        any_checked = False
        for item in ModelItemsIter(self._child_list.model()):
            all_checked = all_checked and bool(item.checkState())
            any_checked = any_checked or  bool(item.checkState())

        self.setStateHandlingSupressed(True)
        self.setCheckState(2 if all_checked else 1 if any_checked else 0)
        self.setStateHandlingSupressed(False)

    def nextCheckState(self):
        if self.checkState() == 2:
            self.setCheckState(0)
        else:
            self.setCheckState(2)

    def eventFilter(self, obj, event: QtCore.QEvent):
        # if event.type() == QtCore.QEvent.KeyRelease:
        #     if event.key() == QtCore.Qt.Key_Return:
        #         if self.hasFocus():
        #             self._child_list.setFocus(True)
        #             # self.clearFocus()

        if (event.type() == QtCore.QEvent.Enter
                or event.type() == QtCore.QEvent.FocusIn):
            # Logger.LOGd(__class__.tag, 'Mouse Enter')
            self._child_list.onCheckBoxEnter()
            # return True

        if (event.type() == QtCore.QEvent.Leave
                or event.type() == QtCore.QEvent.FocusOut):
            # Logger.LOGd(__class__.tag, 'Mouse Leave')
            self._child_list.onCheckBoxLeave()
            # return True

        return super().eventFilter(obj, event)
