# Dark Souls 3 Cheat Sheet tool
# Copyright (C) 2018  Aleksey Chistov
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# Source code at https://bitbucket.org/Hulvdan/ds3-utyll

from PyQt5 import QtCore, QtGui, QtWidgets
from CustomTheme import *
from Config import *
from ProfileDS3 import *
from DialogNewProfile import *
from DelegateUniversalTree import *
from DelegateTreePlaythrough import *
from DelegateTreeArmor import *
from ActionCallback import *
from DialogExport import *
from DialogImport import *
from DialogRename import *
from DialogConfirm import *
from DialogAbout import *
from RetimerCallback import *
from FilterItemsListView import *
from FilterQCheckBox import *
import FileData


class GUI(QtWidgets.QMainWindow):
    tag = 'GUI'
    def __init__(self, *args):
        # Need to set window size and position, update trees correctly
        self._initialised = False
        QtWidgets.QMainWindow.__init__(self, *args)
        uic.loadUi(FileData.filename_gui, self)
        self._title = self.windowTitle()
        self._tree_delegates = []

        self._config = Config(FileData.fileConfigDefault())
        self._custom_theme = CustomTheme(self, QtCore.Qt.Key_F5, QtCore.Qt.Key_F6)

        if not os.path.exists(FileData.ds3_data_profile + '.' + FileData.ext_profile):
            if not ProfileDS3.downloadFile(FileData.ds3_data_profile + '.' + FileData.ext_profile):
                text = GUI.tag + ": No '{}' file. Can't download from\n'{}'.\nShutting down".format(
                    FileData.ds3_data_profile + '.' + FileData.ext_profile, FileData.remote_profile_link)

                self._custom_theme.updateStyle()
                DialogMessage.execute(text, True, self)
                quit()


        self._game_mode_items_layout = QtWidgets.QHBoxLayout(self)
        self.new_game_mode_radio           = QtWidgets.QRadioButton(self)
        self.new_game_mode_plus_radio      = QtWidgets.QRadioButton(self)
        self.new_game_mode_plus_plus_radio = QtWidgets.QRadioButton(self)
        self.new_game_mode_radio.setText('NG')
        self.new_game_mode_plus_radio.setText('NG+')
        self.new_game_mode_plus_plus_radio.setText('NG++')
        self.new_game_mode_radio.setObjectName('radioNG')
        self.new_game_mode_plus_radio.setObjectName('radioNGP')
        self.new_game_mode_plus_plus_radio.setObjectName('radioNGPP')
        self._game_mode_items_layout.addWidget(self.new_game_mode_radio)
        self._game_mode_items_layout.addWidget(self.new_game_mode_plus_radio)
        self._game_mode_items_layout.addWidget(self.new_game_mode_plus_plus_radio)
        if os.path.exists(FileData.fileProfile(self._config.activeProfile)):
            profile_name = self._config.activeProfile
            self._setProfile(FileData.fileProfile(profile_name), False)
        else:
            profile_name = FileData.default_profile
            self._setProfile(FileData.fileProfile(profile_name), False)
            self.profile.save()

        self._updateTitle()

        self._list_items_quests       = FilterItemsListView(self)
        self._list_items_upgrades     = FilterItemsListView(self)
        self._list_items_achievements = FilterItemsListView(self)
        self._list_items_gear         = FilterItemsListView(self)
        self._list_items_materials    = FilterItemsListView(self)
        self._list_items_others       = FilterItemsListView(self)
        self._items_lists = (
            self._list_items_quests,
            self._list_items_upgrades,
            self._list_items_achievements,
            self._list_items_gear,
            self._list_items_materials,
            self._list_items_others
        )

        self.checkQuests        = FilterQCheckBox(self._list_items_quests, self.filterBox)
        self.checkUpgrades      = FilterQCheckBox(self._list_items_upgrades, self.filterBox)
        self.checkAchievements  = FilterQCheckBox(self._list_items_achievements, self.filterBox)
        self.checkGear          = FilterQCheckBox(self._list_items_gear, self.filterBox)
        self.checkMaterials     = FilterQCheckBox(self._list_items_materials, self.filterBox)
        self.checkOthers        = FilterQCheckBox(self._list_items_others, self.filterBox)

        self._list_items_quests.setWidgetForAnchor(self.checkQuests)
        self._list_items_upgrades.setWidgetForAnchor(self.checkUpgrades)
        self._list_items_achievements.setWidgetForAnchor(self.checkAchievements)
        self._list_items_gear.setWidgetForAnchor(self.checkGear)
        self._list_items_materials.setWidgetForAnchor(self.checkMaterials)
        self._list_items_others.setWidgetForAnchor(self.checkOthers)

        self.checkBoss      = QtGui.QStandardItem()
        self.checkNPC       = QtGui.QStandardItem()
        self.checkEstus     = QtGui.QStandardItem()
        self.checkBone      = QtGui.QStandardItem()
        self.checkTome      = QtGui.QStandardItem()
        self.checkCoal      = QtGui.QStandardItem()
        self.checkAsh       = QtGui.QStandardItem()
        self.checkGesture   = QtGui.QStandardItem()
        self.checkSorcery   = QtGui.QStandardItem()
        self.checkPyromancy = QtGui.QStandardItem()
        self.checkMiracle   = QtGui.QStandardItem()
        self.checkRing      = QtGui.QStandardItem()
        self.checkWeapon    = QtGui.QStandardItem()
        self.checkArmor     = QtGui.QStandardItem()
        self.checkTitanite  = QtGui.QStandardItem()
        self.checkGem       = QtGui.QStandardItem()
        self.checkCovenant  = QtGui.QStandardItem()
        self.checkMisc      = QtGui.QStandardItem()

        self._checkboxes_quests = (
            self.checkBoss,
            self.checkNPC
        )
        self._checkboxes_upgrades = (
            self.checkEstus,
            self.checkBone,
            self.checkTome,
            self.checkCoal,
            self.checkAsh
        )
        self._checkboxes_achievements = (
            self.checkGesture,
            self.checkSorcery,
            self.checkPyromancy,
            self.checkMiracle,
            self.checkRing
        )
        self._checkboxes_gear = (
            self.checkWeapon,
            self.checkArmor
        )
        self._checkboxes_materials = (
            self.checkTitanite,
            self.checkGem
        )
        self._checkboxes_others = (
            self.checkCovenant,
            self.checkMisc
        )

        self._model_list_quests       = CustomModel()
        self._model_list_upgrades     = CustomModel()
        self._model_list_achievements = CustomModel()
        self._model_list_gear         = CustomModel()
        self._model_list_materials    = CustomModel()
        self._model_list_others       = CustomModel()
        models = (
            self._model_list_quests,
            self._model_list_upgrades,
            self._model_list_achievements,
            self._model_list_gear,
            self._model_list_materials,
            self._model_list_others
        )

        # for checkboxes_list in lists:
        #     double_clicking_lambda = (lambda x: (checkboxes_list.model().itemFromIndex(x).setCheckState(0 if bool(checkboxes_list.model().itemFromIndex(x).checkState()) else 2)))
        #     checkboxes_list.doubleClicked.connect(double_clicking_lambda)

        # for checkbox in self._check

        self._initConnections()
        self._initFilterCheckboxes()

        self._list_items_quests.setModel(self._model_list_quests)
        self._list_items_upgrades.setModel(self._model_list_upgrades)
        self._list_items_achievements.setModel(self._model_list_achievements)
        self._list_items_gear.setModel(self._model_list_gear)
        self._list_items_materials.setModel(self._model_list_materials)
        self._list_items_others.setModel(self._model_list_others)
        for model in models:
            model.stateChanged.connect(self._onStateChanged)

        self._initMenu()

        # Setting timer that will update current tree after
        # some time of window resizing for reducing performance usage
        duration = 200 # [ms]
        self._treeUpdateOnResizeTimer = QtCore.QTimer(self)
        resize_lambda = lambda: self._currentTreeDelegate().update()
        timeout_lambda = lambda: self._treeUpdateOnResizeRetimerCallback.update(self._treeUpdateOnResizeTimer.interval())
        self._treeUpdateOnResizeRetimerCallback = RetimerCallback(duration, resize_lambda)
        self._treeUpdateOnResizeTimer.timeout.connect(timeout_lambda)
        self._treeUpdateOnResizeTimer.start(duration)

        self.show()
        if self._config.rememberWindowSizePos:
            super().resize(self._config.size[0], self._config.size[1])
            super().move(self._config.position[0], self._config.position[1])

        self._tree_delegates = [
            DelegateTreePlaythrough
            (self, lambda: self.profile.playthrough_data, self.treePlaythrough, self.treePlaythrough_scroll, 0),
            DelegateUniversalTree
            (self, lambda: self.profile.achievements_data, self.treeAchievements, self.treeAchievements_scroll, 1),
            DelegateUniversalTree
            (self, lambda: self.profile.weaponsandshields_data, self.treeWeapons, self.treeWeapons_scroll, 2),
            DelegateTreeArmor
            (self, lambda: self.profile.armor_data, self.treeArmor, self.treeArmor_scroll, 3),
            DelegateUniversalTree
            (self, lambda: self.profile.misc_data, self.treeMisc, self.treeMisc_scroll, 4)
        ]
        for tree_delegate in self._tree_delegates:
            self._custom_theme.addUpdatableObject(tree_delegate)
        self._custom_theme.updateStyle()

        self._tweakTabOrder()
        self._initialised = True

    def _tweakTabOrder(self):
        order = (self.tabWidget,
                 self.checkQuests,
                 self.checkUpgrades,
                 self.checkAchievements,
                 self.checkGear,
                 self.checkMaterials,
                 self.checkOthers,
                 self.new_game_mode_radio,
                 self.treePlaythrough)

        for i in range(len(order)-1):
            self.setTabOrder(order[i], order[i+1])

    @QtCore.pyqtSlot(QtGui.QStandardItem)
    def _onStateChanged(self, item:QtGui.QStandardItem):
        # if item == self.checkQuests:
        #     self._tree_delegates[0].onToggleCheckQuests()
        # if item == self.checkUpgrades:
        #     self._tree_delegates[0].onToggleCheckUpgrades()
        # if item == self.checkAchievements:
        #     self._tree_delegates[0].onToggleCheckAchievements()
        # if item == self.checkGear:
        #     self._tree_delegates[0].onToggleCheckGear()
        # if item == self.checkMaterials:
        #     self._tree_delegates[0].onToggleCheckMaterials()
        # if item == self.checkOthers:
        #     self._tree_delegates[0].onToggleCheckOthers()
        if item == self.checkBoss:
            self._tree_delegates[0].onToggleCheckBoss()
            self.checkQuests.updateState()
        if item == self.checkNPC:
            self._tree_delegates[0].onToggleCheckNPC()
            self.checkQuests.updateState()
        if item == self.checkEstus:
            self._tree_delegates[0].onToggleCheckEstus()
            self.checkUpgrades.updateState()
        if item == self.checkBone:
            self._tree_delegates[0].onToggleCheckBone()
            self.checkUpgrades.updateState()
        if item == self.checkTome:
            self._tree_delegates[0].onToggleCheckTome()
            self.checkUpgrades.updateState()
        if item == self.checkCoal:
            self._tree_delegates[0].onToggleCheckCoal()
            self.checkUpgrades.updateState()
        if item == self.checkAsh:
            self._tree_delegates[0].onToggleCheckAsh()
            self.checkUpgrades.updateState()
        if item == self.checkGesture:
            self._tree_delegates[0].onToggleCheckGesture()
            self.checkAchievements.updateState()
        if item == self.checkSorcery:
            self._tree_delegates[0].onToggleCheckSorcery()
            self.checkAchievements.updateState()
        if item == self.checkPyromancy:
            self._tree_delegates[0].onToggleCheckPyromancy()
            self.checkAchievements.updateState()
        if item == self.checkMiracle:
            self._tree_delegates[0].onToggleCheckMiracle()
            self.checkAchievements.updateState()
        if item == self.checkRing:
            self._tree_delegates[0].onToggleCheckRing()
            self.checkAchievements.updateState()
        if item == self.checkWeapon:
            self._tree_delegates[0].onToggleCheckWeapon()
            self.checkGear.updateState()
        if item == self.checkArmor:
            self._tree_delegates[0].onToggleCheckArmor()
            self.checkGear.updateState()
        if item == self.checkTitanite:
            self._tree_delegates[0].onToggleCheckTitanite()
            self.checkMaterials.updateState()
        if item == self.checkGem:
            self._tree_delegates[0].onToggleCheckGem()
            self.checkMaterials.updateState()
        if item == self.checkCovenant:
            self._tree_delegates[0].onToggleCheckCovenant()
            self.checkOthers.updateState()
        if item == self.checkMisc:
            self._tree_delegates[0].onToggleCheckMisc()
            self.checkOthers.updateState()


    def getCustomTheme(self):
        return self._custom_theme

    def getConfig(self):
        return self._config

    def _initFilterCheckboxes(self):
        main_checkboxes = (
            self.checkQuests,
            self.checkUpgrades,
            self.checkAchievements,
            self.checkGear,
            self.checkMaterials,
            self.checkOthers
        )
        checkboxes = (
            self.checkBoss,
            self.checkNPC,
            self.checkEstus,
            self.checkBone,
            self.checkTome,
            self.checkCoal,
            self.checkAsh,
            self.checkGesture,
            self.checkSorcery,
            self.checkPyromancy,
            self.checkMiracle,
            self.checkRing,
            self.checkWeapon,
            self.checkArmor,
            self.checkTitanite,
            self.checkGem,
            self.checkCovenant,
            self.checkMisc
        )

        self._list_items_quests.setHidden(True)
        self._list_items_upgrades.setHidden(True)
        self._list_items_achievements.setHidden(True)
        self._list_items_gear.setHidden(True)
        self._list_items_materials.setHidden(True)
        self._list_items_others.setHidden(True)

        self.checkQuests.setText('Quests')
        self.checkUpgrades.setText('Upgrades')
        self.checkAchievements.setText('Achievements')
        self.checkGear.setText('Gear')
        self.checkMaterials.setText('Materials')
        self.checkOthers.setText('Others')

        self.checkBoss.setText('Bosses')
        self.checkNPC.setText('NPCs')
        self.checkEstus.setText('Estus Shards')
        self.checkBone.setText('Bone Shards')
        self.checkTome.setText(r'Tomes/Scrolls')
        self.checkCoal.setText('Coals')
        self.checkAsh.setText('Ashes')
        self.checkGesture.setText('Gestures')
        self.checkSorcery.setText('Sorceries')
        self.checkPyromancy.setText('Pyromancies')
        self.checkMiracle.setText('Miracles')
        self.checkRing.setText('Rings')
        self.checkWeapon.setText('Weapons')
        self.checkArmor.setText('Armor')
        self.checkTitanite.setText('Titanite')
        self.checkGem.setText('Gems')
        self.checkCovenant.setText('Covenant items')
        self.checkMisc.setText('Misc')

        for checkbox in checkboxes:
            checkbox.setCheckable(True)
            checkbox.setEditable(False)

        for i in range(len(main_checkboxes)):
            checkbox = main_checkboxes[i]
            # checkbox.setEditable(False)
            checkbox.setCheckable(True)
            self.gridLayout_7.addWidget(checkbox, 0, i)

        self.gridLayout_7.addLayout(self._game_mode_items_layout, 0, len(main_checkboxes))
        # self.gridLayout_7.addWidget(self._new_game_slider, 0, 6)
        spacer = QtWidgets.QSpacerItem(0, 0, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_7.addItem(spacer, 0, len(main_checkboxes)+1)

        self.updateCheckboxesStates()

        for checkbox in self._checkboxes_quests:
            self._model_list_quests.appendRow(checkbox)
        for checkbox in self._checkboxes_upgrades:
            self._model_list_upgrades.appendRow(checkbox)
        for checkbox in self._checkboxes_achievements:
            self._model_list_achievements.appendRow(checkbox)
        for checkbox in self._checkboxes_gear:
            self._model_list_gear.appendRow(checkbox)
        for checkbox in self._checkboxes_materials:
            self._model_list_materials.appendRow(checkbox)
        for checkbox in self._checkboxes_others:
            self._model_list_others.appendRow(checkbox)

    def updateCheckboxesStates(self):
        self.checkBoss.setCheckState(2 if self.profile.showBosses else 0)
        self.checkNPC.setCheckState(2 if self.profile.showNPCs else 0)
        self.checkEstus.setCheckState(2 if self.profile.showEstusShards else 0)
        self.checkBone.setCheckState(2 if self.profile.showBones else 0)
        self.checkTome.setCheckState(2 if self.profile.showTomes else 0)
        self.checkCoal.setCheckState(2 if self.profile.showCoals else 0)
        self.checkAsh.setCheckState(2 if self.profile.showAshes else 0)
        self.checkGesture.setCheckState(2 if self.profile.showGestures else 0)
        self.checkSorcery.setCheckState(2 if self.profile.showSorceries else 0)
        self.checkPyromancy.setCheckState(2 if self.profile.showPyromancies else 0)
        self.checkMiracle.setCheckState(2 if self.profile.showMiracles else 0)
        self.checkRing.setCheckState(2 if self.profile.showRings else 0)
        self.checkWeapon.setCheckState(2 if self.profile.showWeapons else 0)
        self.checkArmor.setCheckState(2 if self.profile.showArmor else 0)
        self.checkTitanite.setCheckState(2 if self.profile.showTitanite else 0)
        self.checkGem.setCheckState(2 if self.profile.showGems else 0)
        self.checkCovenant.setCheckState(2 if self.profile.showCovenants else 0)
        self.checkMisc.setCheckState(2 if self.profile.showMiscItems else 0)

        state_quests = 2 if all(bool(i.checkState()) for i in self._checkboxes_quests) \
            else 1 if any(bool(i.checkState()) for i in self._checkboxes_quests) else 0
        state_upgrades = 2 if all(bool(i.checkState()) for i in self._checkboxes_upgrades) \
            else 1 if any(bool(i.checkState()) for i in self._checkboxes_upgrades) else 0
        state_achievements = 2 if all(bool(i.checkState()) for i in self._checkboxes_achievements) \
            else 1 if any(bool(i.checkState()) for i in self._checkboxes_achievements) else 0
        state_gear = 2 if all(bool(i.checkState()) for i in self._checkboxes_gear) \
            else 1 if any(bool(i.checkState()) for i in self._checkboxes_gear) else 0
        state_materials = 2 if all(bool(i.checkState()) for i in self._checkboxes_materials) \
            else 1 if any(bool(i.checkState()) for i in self._checkboxes_materials) else 0
        state_others = 2 if all(bool(i.checkState()) for i in self._checkboxes_others) \
            else 1 if any(bool(i.checkState()) for i in self._checkboxes_others) else 0

        main_checkboxes = (
            self.checkQuests,
            self.checkUpgrades,
            self.checkAchievements,
            self.checkGear,
            self.checkMaterials,
            self.checkOthers
        )

        for checkbox in main_checkboxes:
            checkbox.setStateHandlingSupressed(True)

        self.checkQuests.setCheckState(state_quests)
        self.checkUpgrades.setCheckState(state_upgrades)
        self.checkAchievements.setCheckState(state_achievements)
        self.checkGear.setCheckState(state_gear)
        self.checkMaterials.setCheckState(state_materials)
        self.checkOthers.setCheckState(state_others)

        for checkbox in main_checkboxes:
            checkbox.setStateHandlingSupressed(False)


    def _initMenu(self):
        self.menuSwitch_to.clear()
        self.menuDelete.clear()

        profiles = FileData.getProfiles()
        for profile_name, profile_filename in profiles:
            action_switch_lambda = lambda x: self.profile.save() or self._setProfile(x, False)
            action_switch = ActionCallback(profile_name, action_switch_lambda, profile_filename,
                                           parent=self.menuSwitch_to)
            self.menuSwitch_to.addAction(action_switch)

            action_del = ActionCallback(profile_name, self._deleteProfile, profile_filename, parent=self.menuDelete)
            self.menuDelete.addAction(action_del)

        act = QtWidgets.QAction(FileData.menuSTNew, self.menuSwitch_to)
        act.triggered.connect(self._createNewProfile)
        act.setShortcut(QtGui.QKeySequence(QtCore.Qt.CTRL + QtCore.Qt.Key_N))
        self.menuSwitch_to.addAction(act)

    def _currentTreeDelegate(self):
        tab = self.profile.getCurrentTab()
        return self._tree_delegates[tab]

    def _reinitTreeDelegates(self):
        for tree in self._tree_delegates:
            tree.reinit()

    def _updateTreeDelegates(self):
        for tree in self._tree_delegates:
            tree.update()

    def getProfile(self) -> ProfileDS3:
        return self.profile

    def _setProfile(self, profile_path, save=True):
        self.profile = ProfileDS3(profile_path)
        if save:
            self.profile.save()
        self._config.activeProfile = FileData.getProfileName(profile_path)
        self._updateTitle()

        self.actionShowURLslist.setChecked(self.profile.show_urls_list)
        self.actionShowCompleted.setChecked(self.profile.showCompleted)
        self.tabWidget.setCurrentIndex(self.profile.getCurrentTab())
        self._reinitTreeDelegates()

        if self.profile.journey == 3:
            self.new_game_mode_plus_plus_radio.setChecked(True)
        elif self.profile.journey == 2:
            self.new_game_mode_plus_radio.setChecked(True)
        elif self.profile.journey == 1:
            self.new_game_mode_radio.setChecked(True)

        if self._initialised:
            self.updateCheckboxesStates()

    def _updateTitle(self):
        self.setWindowTitle(self._title + ' - ' + FileData.getProfileName(self.profile.filename)
                            + ' [v{}]'.format(FileData.app_version))

    def _renameProfile(self):
        new_name = DialogRename.execute(FileData.getProfileName(self.profile.filename), self)
        if new_name == '':
            return

        prev_filename = self.profile.filename
        if os.path.exists(prev_filename):
            os.remove(prev_filename)

        self.profile.filename = FileData.fileProfile(new_name)
        Logger.LOGi(GUI.tag, "renamed '{}' to '{}'", FileData.getProfileName(prev_filename), new_name)
        self.profile.save()
        self._config.activeProfile = new_name
        self._updateTitle()
        self._initMenu()


    def _deleteProfile(self, filename):
        pr_name = FileData.getProfileName(filename)
        result = bool(DialogConfirm.execute("Delete '{}' profile?".format(pr_name), False, self))
        if not result:
            return

        path = filename
        profiles = FileData.getProfiles()
        if os.path.exists(path):
            os.remove(path)
        Logger.LOGi(GUI.tag, "deleted '{}'", FileData.getProfileName(path))
        if filename == self.profile.filename:
            i = 0
            for m in profiles:
                if filename == m[1]:
                    profiles = profiles[0:i] + profiles[i+1:]
                    break
                i+=1

            if len(profiles) == 0:
                self._setProfile(FileData.fileProfileDefault())
            elif len(profiles) == 1:
                self._setProfile(profiles[0][1])
            else:
                if len(profiles) >= i:
                    self._setProfile(profiles[-len(profiles)+i][1])
                else:
                    self._setProfile(FileData.fileProfileDefault())
            self._reinitTreeDelegates()
        self._initMenu()

    def _createNewProfile(self):
        self.menuSwitch_to.close()
        self.menuDelete.close()
        self.profile.save()
        profile = DialogNewProfile.execute(self)
        if profile is None:
            return
        profile.save()
        self._setProfile(profile.filename)
        self._initMenu()

    def _viewExport(self):
        self.profile.save()
        dialog = DialogExport(self)
        dialog.exec_()
        dialog.deleteLater()

    def _viewImport(self):
        dialog = DialogImport(self)
        result = bool(dialog.exec_())
        dialog.deleteLater()

        if result:
            self.profile.save()
            self._setProfile(self.profile.filename)
            self._initMenu()

    def _viewAbout(self):
        dialog = DialogAbout(self)
        dialog.exec_()
        dialog.deleteLater()

    def _updateProfileContent(self):
        # 0 - json couldn't be decoded
        # 1 - higher version of program is required
        # 2 - no updates
        # 3 - file was successfully updaated
        result = ProfileDS3.updateProfileCreatingFile()
        if result == 3:
            self.profile.updateProfileContent()
            self._reinitTreeDelegates()
        elif result == 2:
            pass
        elif result == 1:
            DialogMessage.execute("Latest version of profile data is not\nsupported by this ds3-utyll version.\nCheck source for releases\n(URL is in about window)", False, self)
        elif result == 0:
            DialogMessage.execute('No updates', False, self)
        else:
            assert False

    def moveEvent(self, e):
        pos = e.pos()
        if self._initialised:
            self._config.position = (pos.x(), pos.y())

    def resizeEvent(self, e):
        size = e.size()
        if self._initialised:
            self._config.size = (size.width(), size.height())

        if self._initialised:
            self._treeUpdateOnResizeRetimerCallback.refresh()
            duration = self._treeUpdateOnResizeTimer.interval()
            self._treeUpdateOnResizeTimer.start(duration)

    def closeEvent(self, event):
        self._save()
        event.accept()

    def _save(self):
        self._currentTreeDelegate().dumpScrollData()
        self._config.save()
        self.profile.save()

    def _onActionShowURLslist(self):
        checked = self.actionShowURLslist.isChecked()
        self.profile.show_urls_list = checked
        if checked:
            self._currentTreeDelegate().onItemSelect()
        self._currentTreeDelegate().update()

    def _changeTab(self, x:int):
        self._currentTreeDelegate().dumpScrollData()
        self.getProfile().setCurrentTab(x)
        self._currentTreeDelegate().reinit()
        self._currentTreeDelegate().loadScrollData()

    def _onActionExpandAll(self):
        self._currentTreeDelegate().expandAll()

    def _onActionCollapseAll(self):
        self._currentTreeDelegate().collapseAll()

    def _initConnections(self):
        self.actionShowCompleted.triggered.connect(lambda: self._currentTreeDelegate().update())
        self.actionUpdateProfileContent.triggered.connect(self._updateProfileContent)
        self.actionExport.triggered.connect(self._viewExport)
        self.actionImport.triggered.connect(self._viewImport)
        self.actionRename.triggered.connect(self._renameProfile)
        self.actionAbout.triggered.connect(self._viewAbout)
        self.actionExit.triggered.connect(QtWidgets.qApp.quit)
        self.tabWidget.currentChanged.connect(self._changeTab)
        self.actionShowURLslist.triggered.connect(self._onActionShowURLslist)
        self.actionExpandAll.triggered.connect(self._onActionExpandAll)
        self.actionCollapseAll.triggered.connect(self._onActionCollapseAll)

    def timerEvent(self, event:QtCore.QTimerEvent):
        timer_id = event.timerId()
        if timer_id == self._treeUpdateOnResizeTimer.timerId():
            event.setAccepted(True)
            dt = self._treeUpdateOnResizeTimer.interval()
            self._treeUpdateOnResizeRetimerCallback.update(dt)
